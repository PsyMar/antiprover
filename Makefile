#This file is part of Antiprover, a suite for playing and generating proof trees for antichess.
#Copyright (C) 2021 JMHB, contact address cflzne_tvgyno@fastmail.com.
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

# updated 2021-01-23

.PHONY: all clean objclean allclean install objects linecount lc truelinecount tlc wc twc itch itchsrc itchdocs test
#remove the .exe if compiling on native linux instead of cygwin
EXECFILE := Antiprover.exe
#compiler name
CC := clang
#compiler flags
#fast
#FLAGS := -Weverything -O3 -D NDEBUG
#debug
FLAGS := -Weverything -O0 -ggdb
#code that tests stuff
TESTS := piecetests.o
#object files produced in compilation
OBJECTS := player.o piece.o piecetype.o square.o rank.o file.o miscutils.o $(TESTS)
# set this to "Makefile" to make everything recompile on a makefile change
# leave it blank to make that not happen.
MAKEFILECHANGE := 
#list of all files
ALLFILES := testcode/*.cpp src/*.cpp headers/*.hpp Doxyfile LICENSE.txt Makefile README.md COPYING

TYPES_HPP := headers/types.hpp
DOCS_HPP := headers/docs.hpp
PLAYER_HPP := headers/player.hpp
PIECETYPE_HPP := headers/piecetype.hpp 
RANK_HPP := headers/rank.hpp $(TYPES_HPP)
PIECE_HPP := headers/piece.hpp $(PIECETYPE_HPP) $(PLAYER_HPP)
MISCUTILS_HPP := headers/miscutils.hpp $(DOCS_HPP)
CTO_HPP := headers/0compile_time_options.hpp
SQUARE_HPP := headers/square.hpp $(CTO_HPP) $(TYPES_HPP)
# All target
all: $(EXECFILE)

test: $(TESTS)

doc/index.html: $(ALLFILES)
	doxygen >doxyout.txt~
	butler push doc psymar/antiprover:docs

# the ~ in the below filename is to keep it from being uploaded by gitignore
itch_update~: $(ALLFILES)
	mkdir release || echo that\'s fine
	mkdir release/src || echo that\'s fine
	mkdir release/headers || echo that\'s fine
	mkdir release/testcode || echo yep lol
	cp -f -t release/src src/*.cpp
	cp -f -t release/headers headers/*.hpp
	cp -f -t release/testcode testcode/*.cpp
	cp -f -t release Doxyfile LICENSE.txt Makefile README.md COPYING
	butler push release psymar/antiprover:sources
	touch itch_update~

itchsrc: itch_update~

itchdocs: doc/index.html 

itch: itchsrc itchdocs

#hope this works.  Want to just make the execfile and leave no cruft behind.
install: all objclean

#count lines in program
linecount:
	wc -l -w -c $(ALLFILES)

lc: linecount

wc: linecount

truelinecount:
	cat $(ALLFILES) >truelinecount.txt
	tr -s -t '\r' '\n' <truelinecount.txt >truelinecount2.txt
	wc -l -w -c <truelinecount2.txt

tlc: truelinecount

twc: truelinecount  

# clean cruft
clean: 
	rm -f *~ truelinecount.txt truelinecount2.txt

objclean: clean
	rm -f *.o

allclean: objclean
	rm -f $(EXECFILE)

# just build object files
objects: $(OBJECTS)

# Main executable, on Linux
Antiprover: $(OBJECTS)
	$(CC) $(FLAGS) -o Antiprover $(OBJECTS)

# Main executable, on Cygwin
Antiprover.exe: $(OBJECTS)
	$(CC) $(FLAGS) -o Antiprover.exe $(OBJECTS)

square.o: src/square.cpp $(SQUARE_HPP) $(MAKEFILECHANGE)
	$(CC) $(FLAGS) -c src/square.cpp

player.o: src/player.cpp $(PLAYER_HPP) $(PIECE_HPP) $(CTO_HPP) $(MAKEFILECHANGE)
	$(CC) $(FLAGS) -c src/player.cpp

piecetype.o: src/piecetype.cpp $(PIECETYPE_HPP) $(PIECE_HPP) $(MAKEFILECHANGE)
	$(CC) $(FLAGS) -c src/piecetype.cpp

piece.o: src/piece.cpp $(PIECE_HPP) $(PLAYER_HPP) $(PIECETYPE_HPP) $(MAKEFILECHANGE) 
	$(CC) $(FLAGS) -c src/piece.cpp

piecetests.o: testcode/piecetests.cpp piece.o piecetype.o player.o miscutils.o
	$(CC) $(FLAGS) -c testcode/piecetests.cpp

miscutils.o: src/miscutils.cpp $(MISCUTILS_HPP)
	$(CC) $(FLAGS) -c src/miscutils.cpp

rank.o: src/rank.cpp $(MISCUTILS_HPP) $(RANK_HPP) $(CTO_HPP)
	$(CC) $(FLAGS) -c src/rank.cpp
