# Antiprover
An antichess suite which will, when finished, be capable of solving positions
with proof-number search as well as playing actual games of antichess and
building endgame tablebases and bitbases.

It will also be capable of proving positions based on a skeletal outline such
as those found at http://magma.maths.usyd.edu.au/~watkins/LOSING_CHESS/lines.html.

Revision history:
0.0.2 (2021-01-?? incomplete): Scrapped almost all code from 0.0.1, so I bumped the number.
0.0.1 (2021-01-15 or so): Wrote a bunch of code with tests and stuff that was bloody awful.  Didn't complete.
2020-12-19: began work on this incarnation of the project.
