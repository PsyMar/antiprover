// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for the PieceType class.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.2 2021-01-15
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_CHESS_PIECETYPE_HPP_
/** @brief piecetype.hpp header define wrapper
 * @since Antiprover 0.0.2 2021-01-15 */
#define JMHB_CHESS_PIECETYPE_HPP_

#include <cstdint>

namespace JMHB {
namespace Chess {

/* forward declarations */
class Piece;
class Player;

/** @brief A class for chess pieces without regard for whose they are.
 * @since Antiprover 0.0.2 2021-01-16 */
class PieceType {
 public:
  /* NO DEFAULT CONSTRUCTOR.  NO.  BAD COMPILER. */
  PieceType() = delete;

  /** @brief Integer type for indexing arrays with pieces.
   * @since Antiprover 0.0.2 2021-01-16 */
  typedef uint8_t index_t;

  /** @brief You know what a pawn is, right?
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr static PieceType PAWN();

  /** @brief sliding piece, moves diagonally.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr static PieceType BISHOP();

  /** @brief sliding piece, moves orthogonally.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr static PieceType KNIGHT();

  /** @brief Please don't call it a castle, it gets confused with the castling
   * move.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr static PieceType ROOK();

  /** @brief His majesty.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr static PieceType KING();

  /** @brief Her grace.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr static PieceType QUEEN();

  /* I considered having a NONE() but if you need that use
   * std::optional<PieceType> */
  /** @brief Convert a character representing a piecetype to that piecetype.
   * @since Antiprover 0.0.2 2021-01-16 */
  explicit constexpr PieceType(char);

  /** @brief Get an upper-case character representing the piecetype.
   * @details good for SAN purposes to avoid bishop/b-file conflict.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr char to_upper_char() const;

  /** @brief Get a lower-case character representing the piecetype.
   * @details Used for black pieces in FEN notation, and sometimes for promotion
   * pieces.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr char to_lower_char() const;

  /** @brief Get a lower-case character representing the piecetype, but rot13'd.
   * @details Used for black pieces in filenames because of windows ignoring
   * case.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr char to_rot13_char() const;

  /** @brief Use to index an array.
   * @details Ranges from 0 to 5 since there are 6 types of piece.  Pawn comes
   * last because reasons
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr index_t index() const;

  /** @brief Combine a Player with a PieceType to get a Piece.
   * @details Operator& and Operator+ are synonymous for these classes.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Piece operator&(const Player& rha) const;

  /** @brief Combine a Player with a PieceType to get a Piece.
   * @details Operator& and Operator+ are synonymous for these classes.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Piece operator+(const Player& rha) const;

  /** @brief compare two PieceTypes for equality.
   * @since Antiprover 0.0.2 2021-01-17 */
  constexpr bool operator==(const PieceType& rha) const;

  /** @brief compare two PieceTypes for inequality.
   * @since Antiprover 0.0.2 2021-01-17 */
  constexpr bool operator!=(const PieceType& rha) const;

 private:
  constexpr PieceType(index_t);
  index_t my_index;
};

}  // namespace Chess
}  // namespace JMHB

#endif /* JMHB_CHESS_PIECETYPE_HPP_ */
