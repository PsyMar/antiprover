#if 0
// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header that defines the Move class for antichess and classes/methods
 * for compressing it.
 * @version Antiprover 0.0.1
 * @date 2021-01-04
 * @since Antiprover 0.0.1 2021-01-01
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file.
 */

#ifndef MOVE_H_
/** @brief move.h header define wrapper
 * @since Antiprover 0.0.1 2021-01-01
 */
#define MOVE_H_

#include <cstdint>
#include <memory>
#include <stdexcept>
#include <vector>

#include "0compile_time_options.hpp"  // for ALLOW_DEBUG/NDEBUG
#include "piece.hpp"
#include "square.hpp"
#include "types.hpp"

struct BitpackedMove;

/** @brief A very basic struct for Moves in a game of antichess.
 * @details Cannot be default-constructed (what values would you even use??).
 * Default copy constructor, assignment operator, etc.  Do I really need to
 * doxygen those?
 * @invariant from() == start() // these are synonyms
 * @invariant to() == end() // these are synonyms
 * @invariant from() != to()
 * @invariant promotion() != Piece::PAWN
 * @invariant !(promotion() & PIECE_COLOR_FLAG)
 * @since Antiprover 0.0.1 2021-01-01
 */
struct Move {
 public:
  /* no default constructor -- zeroing would violate invariant */
  Move() = delete;
  /** @brief Construct a move.
   * @details These parameters are sufficient to determine a unique move --
   * whether the move is a capture, or is legal, depends on the preceding board
   * state.
   * @param fromsq the square from which a piece was moved.
   * @param tosq the square to which a piece was moved.  May not equal fromsq.
   * @param promoted_to is the piece to which a pawn was promoted.  MAY NOT be
   * Piece::PAWN. Defaults to Piece::NONE.  Color information is stripped if the
   * passed-in Piece has it.
   * @throws std::invalid_argument if NDEBUG not defined and arguments break
   * above rules
   * @since Antiprover 0.0.1 2021-01-01
   */
  Move(Square fromsq, Square tosq,
       Piece promoted_to = Piece::NONE) /* throw (
#ifndef NDEBUG
std::invalid_argument
#endif
)*/;

  /** @brief (re)-construct a Move from a BitpackedMove.
   * @since Antiprover 0.0.1 2021-01-02
   */
  static Move extract(const BitpackedMove& BM) /* throw() */;

  /** @brief Retrive the square being moved from.
   * @details Equivalent to Move::from().
   * @since Antiprover 0.0.1 2021-01-01
   */
  Square start() const /* throw() */;

  /** @brief Retrive the square being moved from.
   * @details Equivalent to Move::start().
   * @since Antiprover 0.0.1 2021-01-01
   */
  Square from() const /* throw() */;

  /** @brief Retrieve the square being moved to.
   * @details Equivalent to move::to().
   * @since Antiprover 0.0.1 2021-01-01
   */
  Square end() const /* throw() */;

  /** @brief Retrieve the square being moved to.
   * @details Equivalent to move::end().
   * @since Antiprover 0.0.1 2021-01-01
   */
  Square to() const /* throw() */;

  /** @brief Does this move promote a pawn?
   * @since Antiprover 0.0.1 2021-01-01
   */
  bool is_promotion() const /* throw() */;

  /** @brief What does this move promote a pawn to, if anything?
   * @returns The piece promoted to -- color bits will be set to 0 (white).
   * @since Antiprover 0.0.1 2021-01-01
   */
  Piece promotion() const /* throw() */;

 private:
  bool invariant() const /* throw() */;
  Square from_sq;
  Square to_sq;
  Piece promote_to;
};

/** @brief A bitpacked Move class.  Slower for processing, smaller for I/O (16
 * bits not 24).
 * @details No default constructor. Copy constructor, assignment operator, etc
 * are default. For long arrays of moves, use BitpackedMoveArray.
 * @invariant expand().invariant() == true
 * @since Antiprover 0.0.1 2021-01-01
 */
class BitpackedMove {
 public:
  /* no default constructor -- zeroing would violate the invariant. */
  BitpackedMove() = delete;

  /** @brief construct a BitpackedMove from a Move.
   * @since Antiprover 0.0.1 2021-01-01
   */
  BitpackedMove(const Move& mv) /* throw() */;

  /** @brief Decompress a BitpackedMove to a Move.
   * @since Antiprover 0.0.1 2021-01-01
   */
  Move expand() const /* throw() */;

  /** @brief Extract the bits for writing.
   * @details No guarantees are made about format besides the fact it is 16
   * bits.
   * @since Antiprover 0.0.1 2021-01-02
   */
  uint16_t to_bits() const /* throw() */;

  /** @brief Read a bitpacked move from bits.
   * @details Only guaranteed to work with bits resulting from to_bits().
   * For normal construction purposes, use the BitpackedMove(Move) ctor.
   * @throws std::invalid_argument if NDEBUG not defined and argument invalid
   * @since Antiprover 0.0.1 2021-01-02
   */
  static BitpackedMove from_bits(
      uint16_t) /* throw (
#ifndef NDEBUG
std::invalid_argument
#endif
)*/;

 private:
  BitpackedMove(
      uint16_t) /* throw (
#ifndef NDEBUG
std::invalid_argument
#endif
)*/;
  Square from() const /* throw() */;
  Square to() const /* throw() */;
  Piece prom() const /* throw() */;
  bool invariant() const /* throw() */;
  uint16_t mybits = 0;
};
/* was gonna have a super-bitpacked MoveArray class, but that feature is
 * deferred
 * until I know what I need from it so I don't reproduce std::vector in full */

#endif /* MOVE_H_ */
#endif
