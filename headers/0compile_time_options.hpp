// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for compile-time options as stored in define flags for power
 * users.
 * @details Filename starts with a zero to help users notice this file.
 * Options start at ctrl-F "BELOW THIS LINE" and end at "ABOVE THIS LINE".
 * @version Antiprover 0.0.2
 * @date 2020-01-27
 * @since Antiprover 0.0.1 2020-12-30
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_CHESS_ANTI_COMPILE_TIME_OPTIONS_HPP_
/** @brief 0compile_time_options.hpp header define wrapper
 * @details Can't start it with the 0, so we just elide that.
 * @since Antiprover 0.0.1 2020-12-30
 */
#define JMHB_CHESS_ANTI_COMPILE_TIME_OPTIONS_HPP_

#include "docs.hpp"
// these *do* need to be #defines, not constexprs, to control inclusion of not
// only code but parts of data structures

// BELOW THIS LINE ARE THE USER-CONFIGURABLE OPTIONS, DO NOT EDIT PRIOR TO HERE

/** @brief Are strings constexpr in the libraries yet?
 * @details They aren't in the version of gcc I have as I write this.
 * @since Antiprover 0.0.1 2021-01-13 */
#define JMHB_CONSTEXPR_STRING 0

/** @brief Should we store rotated bitboards?
 * @details 1 for yes, 0 for no.  If true, we store four bitboards
 * simultaneously for accessing files and diagonals more quickly than standard
 * "kindergarten" bitboards, as they are already in an 8-bit integer without
 * doing math first. Updating positions is somewhat slower with this method,
 * however. Repository default TBD but 0 for now (still unimplemented LOL.)
 * @since Antiprover 0.0.1 2020-12-30 */
#define JMHB_CHESS_ANTI_ROTATED_BITBOARDS 0

/** @brief Option for "magic" bitboards.
 * @details Magic bitboards use arithmetic to make either files and ranks, or
 * both diagonals, contiguous in a single integer so there's only one lookup.
 * This, however, is not always faster, partly because of the size of the
 *   lookup tables.  Because of this, there are 5 options:
 *
 * * 0 means do not use magic bitboards.
 * * 1 means use them always.
 * * -1 uses magic for bishops, but not rooks.
 * * N where N>1 means use them only for square/piece-type combinations where
 *   the lookup table indices are <= N bits.
 * * N where N<-1 means the same, except only for bishops (no rooks).
 *   If ROTATED_BITBOARDS is set, non-magic lookups will use them.
 *
 * If N is -1, or is >= the maximum index length for bishops, at most the
 * normal-orientation and sideways rotated bitboards will be stored, not the
 * diagonal ones.
 * If N is 1, or is >= the maximum index length for all pieces, rotated
 * bitboards will not be stored regardless of ROTATED_BITBOARDS as they are
 * not needed.

 * Repository default eventually TBD but 0 for now since they aren't
 * actually implemented LOL.
 * My guess is -1 would work best but I have no clue!
 * And it *is* architecture-dependent what's best.
 * @since Antiprover 0.0.1 2020-12-30 */
#define JMHB_CHESS_ANTI_MAGIC_BITBOARDS 0

/** @def JMHB_CHESS_ANTI_ALLOW_DEBUG
 * @brief Spend cycles on runtime debugging sanity checks?
 * @details positive for yes, 0 for no.  Default in repository is 10 until 1.0
 * released. 10 means very, very, very thorough sanity checks. Once there is a
 * stable version, default in repository will be no.
 * @since Antiprover 0.0.1 2020-12-31 */
#ifndef NDEBUG
#define JMHB_CHESS_ANTI_ALLOW_DEBUG 10
#else
#define JMHB_CHESS_ANTI_ALLOW_DEBUG 0
#endif

// ABOVE THIS LINE ARE THE USER-CONFIGURABLE OPTIONS, DO NOT EDIT AFTER

/*#if MAGIC_BITBOARDS == 1
#undef MAGIC_BITBOARDS
#define MAGIC_BITBOARDS 20
// anything over 16 will do here, probably
#endif*/

/*#if MAGIC_BITBOARDS == -1
#undef MAGIC_BITBOARDS
#define MAGIC_BITBOARDS -20
// anything over 16 will do here, probably
#endif*/

#endif /* JMHB_CHESS_ANTI_COMPILE_TIME_OPTIONS_HPP_ */
