#if 0
// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for the main antichess board class.
 * @version Antiprover 0.0.1
 * @date 2021-01-04
 * @since Antiprover 0.0.1 2021-01-03
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#include <array>
#include <map>
#include <optional>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include "move.h"
#include "piece.hpp"

#ifndef BOARD_H_
/** @brief board.h header define wrapper
 * @since Antiprover 0.0.1 2021-01-03 */
#define BOARD_H_

/** @brief An enumeration of possible game results.
 * @since Antiprover 0.0.1 2021-01-03
 */
enum class Result : uint8_t {
  /** @brief The game is not over!
   * @since Antiprover 0.0.1 2021-01-03
   */
  NOT_OVER = 0,
  /** @brief Game over, nobody wins!
   * @since Antiprover 0.0.1 2021-01-03
   */
  DRAWN = 1,
  /** @brief White wins!
   * @since Antiprover 0.0.1 2021-01-03
   */
  WHITE_WON = 2,
  /** @brief Black wins!
   * @since Antiprover 0.0.1 2021-01-03
   */
  BLACK_WON = 3
};

/** @brief Given a Result variable -- is the game over?
 * @since Antiprover 0.0.1 2021-01-03
 */
bool is_over(Result r) { return r != Result::NOT_OVER; }

/** @brief Given a result variable -- Did it end a draw?
 * @since Antiprover 0.0.1 2021-01-03
 */
bool drawn(Result r) { return r == Result::DRAWN; }

/** @brief Given a result variable -- did white win?
 * @since Antiprover 0.0.1 2021-01-03
 */
bool white_won(Result r) { return r == Result::WHITE_WON; }

/** @brief Given a result variable -- did black win?
 * @since Antiprover 0.0.1 2021-01-03
 */
bool black_won(Result r) { return r == Result::BLACK_WON; }

class Smallboard;
class Square;

/** @brief Class which defines an antichess board.
 * @details Immutable, but you can make a copy with a move made.
 * @since Antiprover 0.0.1 2021-01-03
 */
class Board {
 public:
  /** @brief Initialize board from a Forsythe-Edwards Notation string.
   * @details FEN strings that are in some way impossible to parse (e.g., wrong
   * number of squares on a rank, or something impossible with en passant) will
   * cause an invalid_argument exception.  This exception *may* be thrown if the
   * position can be parsed, but is impossible in normal gameplay (e.g., only
   * pawns on the board and 100-ply counter not zero).
   * @since Antiprover 0.0.1 2021-01-03
   */
  Board(const std::string &FEN =
            "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - - 0 1")
      /* throw(std::invalid_argument) */;

  /** @brief Get a FEN string that represents the board state.
   * @since Antiprover 0.0.1 2021-01-03
   */
  std::string FEN() const /* throw() */;

  /** @brief Initialize board from a map of squares to pieces + misc info.
   * @param pieces A map of Squares to Pieces; missing squares left empty
   * (Piece::NONE).
   * @param is_black_turn True if it is black's turn, false otherwise (default).
   * @param ply_of_100 How many ply (half-moves) are we towards the 50-move
   * rule? Technically the game doesn't end until 150 but we just assume a draw
   * will be claimed at 100.
   * @param en_passant Either the File on which en passant is legal, or
   * if en passant is not legal, std::nullopt (the default)
   * @note If pieces contains only pawns, ply_of_100 should be 0 (default), as
   * the last move was clearly a pawn move, and may have been a capture as well.
   * @since Antiprover 0.0.1 2021-01-03
   */
    Board(const std::map<Square, Piece> & pieces,
        bool is_black_turn = false, uint8_t ply_of_100 = 0,
        std::optional<File> en_passant = std::nullopt))
        /* throw(std::invalid_argument) */;

    /** @brief Initialize board from an array mapping squares to pieces,
        + misc info.
     * @param pieces An array mapping Square s to Piece s or Piece::NONE.
     * @param is_black_turn True if it is black's turn, false otherwise
     (default).
     * @param ply_of_100 How many ply (half-moves) are we towards the 50-move
     rule?
     * Technically the game doesn't end until 150 but we just assume a draw will
     * be claimed at 100.
     * @param en_passant Either the File on which en passant is legal, or
     * if en passant is not legal, std::nullopt (the default).
     * @note If pieces contains only pawns, ply_of_100 should be 0 (default), as
     * the last move was clearly a pawn move, and may have been a capture as
     well.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Board(const std::array<Piece, Board::NUM_SQUARES> &pieces,
          bool is_black_turn = false, uint8_t ply_of_100 = 0,
          std::optional<File> en_passant = std::nullopt)
        /* throw(std::invalid_argument) */;

    /** @brief Get a set of legal moves from the current position.
     * @details A lot goes into this.  Use it wisely.  Moves ordered by
     * Move::operator<
     * @since Antiprover 0.0.1 2021-01-03
     */
    std::set<Move> legal_moves() const /* throw() */;

    /** @brief Convert the board to a human-readable string.
     * @details Uses uppercase letters for one player and lowercase for the
     * other. Empty squares are spaces, | _ and + used to separate squares. En
     * passant/turn info displayed at bottom.
     * @since Antiprover 0.0.1 2021-01-03
     */
    std::string to_display_string() const /* throw() */;

    /** @brief Get the result of the game.
     * @details If not game_over(), returns Result::NOT_OVER.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Result game_result() const /* throw() */;

    /** @brief Is the game over?
     * @details Equivalent to is_over(game_result()).
     * @since Antiprover 0.0.1 2021-01-03
     */
    bool game_over() const /* throw() */;

    /** @brief Make a move *on this board* and return the result.
     * @details If the move was not generated from this board's position,
     * it may be an illegal move, resulting in an invalid_argument exception.
     * This version modifies the Board; a const version also exists.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Board &make_move(const Move &mv) /* throw(std::invalid_argument) */;

    /**
     * @brief Make a move and return the resulting position.
     * @details If the move was not generated from this board's position,
     * it may be an illegal move, resulting in an invalid_argument exception.
     * The board object invoking this is not modified, rather a new board is
     * returned.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Board make_move(const Move &mv) const /* throw(std::invalid_argument) */;

    /** @brief Make a series of moves *on this board* and return it.
     * @details if any of the moves are illegal, results in an invalid_argument
     * exception. There's also a const version which makes the moves on a cloned
     * board.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Board &make_moves(const std::vector<Move> &moves)
        /* throw(std::invalid_argument) */;

    /**
     * @brief Return the position resulting from a series of moves, starting
     * from this Board.
     * @details This version doesn't modify the board; the non-const version
     * does. See Board::make_move(Move) for more details.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Board make_moves(const std::vector<Move> &moves) const
        /* throw(std::invalid_argument) */;

    /** @brief Get this position with flipped colors.
     * @details E.g. if moves are 1.e3 e5, this will give the position
     * after 1.e3 e6 2.e4. (These are not good moves, it's just an example.)
     * @since Antiprover 0.0.1 2021-01-03
     */
    Board flip_colors() const /* throw() */;

    /** @brief Is en passant legal in the current position?
     * @note If it is, we shouldn't check this position in tablebases.
     * @since Antiprover 0.0.1 2021-01-03
     */
    bool en_passant_legal() const /* throw() */;

    /** @brief How many pieces are left for both players?
     * @note Useful for determining whether this position is tablebase-ready.
     * @since Antiprover 0.0.1 2021-01-03
     */
    uint8_t pieces_left() const /* throw() */;

    /** @brief At how many ply is the 100-ply (50-move) counter?
     * @since Antiprover 0.0.1 2021-01-03
     */
    uint8_t ply_towards_draw() const /* throw() */;

    /** @brief Get a hash of this position.
     * @details Value is constant for board in a given run of the program.
     * Value is *not* constant for board between runs of the program.
     * Does not hash the 100-ply counter.  Query that separately.
     * @since Antiprover 0.0.1 2021-01-03
     */
    uint64_t get_hash() const /* throw() */;

    /** @brief Get this board, compressed into a smaller struct of 33 bytes.
     * @details Compression is designed to be reasonably quick, but not as quick
     * as checking the hash value, so only use this if you want to be absolutely
     * sure.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Smallboard compress() const /* throw() */;

    /** @brief Decompress a Smallboard.
     * @since Antiprover 0.0.1 2021-01-03
     */
    Board(const Smallboard &) /* throw() */;

    /** @brief Comparison operator so you can have sets of boards.
     * @details Compares all traits of the board, except the hundred-ply count
     * that is left out of hashing.
     * @since Antiprover 0.0.1 2021-01-03
     */
    bool operator<(const Board &rha) const /* throw() */;

    /** @brief Compare whether two boards are equal -- ignoring hundred-ply
     * count.
     * @since Antiprover 0.0.1 2021-01-03
     */
    bool operator==(const Board &rha) const /* throw() */;

    /** @brief Compare whether two boards are unequal -- ignoring hundred-ply
     * count.
     * @since Antiprover 0.0.1 2021-01-03
     */
    bool operator!=(const Board &rha) const /* throw() */;

    /** @brief Number of ranks on a chessboard.
     * @details Really I don't think anyone would blink if I just put 8 in my
     * code, but I feel this is good practice.
     * @since Antiprover 0.0.1 2021-01-04
     */
    constexpr uint8_t NUM_RANKS = 8;
    /** @brief Number of files on a chessboard.
     * @details Really I don't think anyone would blink if I just put 8 in my
     * code, but I feel this is good practice.
     * @since Antiprover 0.0.1 2021-01-04
     */
    constexpr uint8_t NUM_FILES = 8;

    /** @brief Number of squares on a chessboard.
     * @details Really I don't think anyone would blink if I just put 64 in my
     * code, but I feel this is good practice.
     * @since Antiprover 0.0.1 2021-01-04
     */
    constexpr uint8_t NUM_SQUARES = NUM_RANKS * NUM_FILES;

   private:
    int parse_FEN_rank(const std::string &FEN,
                       std::array<Piece, Board::NUM_SQUARES> &pieces, Rank rank,
                       int startloc) /* throw(std::invalid_argument) */;
    std::array<bitboard_t, 7> white_pieces;
    std::array<bitboard_t, 7> black_pieces;
    uint64_t hash;
    uint8_t hundred_ply_count;
    std::optional<File>
        ep_file; /* if not std::nullopt, hundred_ply_count == 0 */
    bool black_to_move;
    static std::array<std::array<uint64_t, Board::NUM_SQUARES>, 6>
        white_piece_hashes;
    static std::array<std::array<uint64_t, Board::NUM_SQUARES>, 6>
        black_piece_hashes;
    static std::array<uint64_t, Board::NUM_FILES> ep_file_hashes;
    static uint64_t black_to_move_hash;
};

/** @brief A compressed Board, not suitable for making moves on.
 * @since Antiprover 0.0.1 2021-01-03
 */
class Smallboard {
 public:
  friend Board::Board(const Smallboard &);
  friend Smallboard Board::compress() const;
  /** @brief Create a Smallboard from a Board.
   * @since Antiprover 0.0.1 2021-01-03
   */
  Smallboard(Board b = Board()) /* throw() */;

  /** @brief Expand a Smallboard to a Board.
   * @since Antiprover 0.0.1 2021-01-03
   */
  Board expand() const /* throw() */;

  /** @brief Compare two smallboards so you can put them in sets and stuff.
   * @details Order not guaranteed to be the same as Board.
   * Ignores 50-move counter but includes who-to-move and en passant file.
   * @since Antiprover 0.0.1 2021-01-03
   */
  bool operator<(const Smallboard &sb) const /* throw() */;

  /** @brief Compare two Smallboards for equality.
   * @details Order not guaranteed to be the same as Board.
   * Ignores 50-move counter but includes who-to-move and en passant file.
   * @since Antiprover 0.0.1 2021-01-03
   */
  bool operator==(const Smallboard &sb) const /* throw() */;

  /** @brief Compare two Smallboards for non-equality.
   * @details Order not guaranteed to be the same as Board.
   * Ignores 50-move counter but includes who-to-move and en passant file.
   * @since Antiprover 0.0.1 2021-01-03
   */
  bool operator!=(const Smallboard &sb) const /* throw() */;

 private:
  bitboard_t knpboard;
  bitboard_t qrkboard;
  bitboard_t qbnboard;
  bitboard_t allwhiteboard;
  uint8_t btm100ep; /* encodes who to move, 100-ply, and ep file */
};

#endif /* BOARD_H_ */
#endif
