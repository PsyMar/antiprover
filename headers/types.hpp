// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header with integer and enumerated types used in the program.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.1 2020-12-28
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_CHESS_TYPES_HPP_
/** @brief types.hpp header define wrapper
 * @since Antiprover 0.0.1 2020-12-28 */
#define JMHB_CHESS_TYPES_HPP_

#include <cstdint>

namespace JMHB {
namespace Chess {
/** @brief Type used for chess bitboards with one bit per square.
 * @since Antiprover 0.0.1 2020-12-30 */
typedef uint64_t bitboard_t;

/** @brief type used for single ranks or files of chess bitboards with one bit
 * per square.
 * @since antiprover 0.0.2 2021-01-13 */
typedef uint8_t bitrank_t;

/** @brief type used for single ranks or files of chess bitboards with one bit
 * per square.
 * @since antiprover 0.0.2 2021-01-13 */
typedef bitrank_t bitfile_t;

}  // namespace Chess
}  // namespace JMHB

#endif /* JMHB_CHESS_TYPES_HPP_ */
