// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for miscellaneous utility functions.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.1 2020-12-31
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_MISCUTILS_HPP_
/** @brief miscutils.hpp header define wrapper
 * @since Antiprover 0.0.1 2020-12-31 */
#define JMHB_MISCUTILS_HPP_

#include <string>

#include "docs.hpp"

namespace JMHB {

/** @brief 13-letter Caesar shift cypher, for individual characters.
 * @details Maintains case.  If not letter, returns argument char unchanged.
 * @since Antiprover 0.0.1 2020-12-31 */
constexpr char rot13(char ch);

/** @brief A constexpr void* which evaluates to false.
 * @details This is rather easy compared to ::JMHB::VOID_TRUE.
 * You want these because void* implicit-converts to bool but not int (unlike
 * bool which implicit-converts to int).
 * @see VOID_TRUE
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* VOID_FALSE = nullptr;

/** @brief Target for ::JMHB::VOID_TRUE to point to.
 * @details The value of this pointer is completely irrelevant.  it's defined in
 * miscutils.cpp Since it isn't constexpr, its address is a constant known at
 * compile-time, storable in a constexpr. Since it's a void*, its address is a
 * void**, which collapses to void*. Thus its address is a constexpr void* which
 * evaluates to true (since it isn't null).
 * @see VOID_TRUE
 * @since Antiprover 0.0.2 2021-01-19 */
extern const void* VOID_TRUE_TARGET;

/** @brief a constexpr void* which evaluates to true.
 * @details Surprisingly tricky to engineer; I came up with this solution after
 * seeing another one that relied on asm nonse. You want these because void*
 * implicit-converts to bool but not int (unlike bool which implicit-converts to
 * int).
 * @see VOID_TRUE_TARGET
 * @see VOID_FALSE
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* VOID_TRUE = &VOID_TRUE_TARGET;

}  // namespace JMHB

#endif /* JMHB_MISCUTILS_HPP_ */
