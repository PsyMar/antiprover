// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for the File class.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.2 2021-01-20
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_CHESS_FILE_HPP
/** @brief file.hpp header define wrapper
 * @since Antiprover 0.0.2 2021-01-20 */
#define JMHB_CHESS_FILE_HPP

#include "types.hpp"

namespace JMHB {
namespace Chess {

class Square;

/** @brief A class for Files of a chessboard.
 * @sa Rank
 * @sa Square
 * @since Antiprover 0.0.2 2021-01-19 */
class File {
 public:
  /** @brief type used as an index for Files
   * @since Antiprover 0.0.2 2021-01-19 */
  using index_t = int;

  /** @brief File - File -> File::Delta
   * @details Positive value -> right (white's POV)
   * Negative -> left (white's POV)
   * Zero -> no change
   * @note defines way more operators than are realistically gonna be used.
   * @since Antiprover 0.0.2 2021-01-19 */
  class Delta;

  /* NO default constructor */
  File() = delete;

  /** @brief The number of Files on a chessboard.
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr index_t NUM_FILES = 8;
  /** @brief Convert a character to a File.
   * @details 'a' -> the leftmost File, 'b' -> the second leftmost File, up to
   * 'h' -> the rightmost File. Other characters will throw an invalid_argument
   * exception.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr File(char);

  /* to avoid clutter operator doc comments go in File.cpp */
  constexpr File operator+(const Delta& rha) const;
  File& operator+=(const Delta& rha);
  File& operator++();               /* prefix */
  File operator++(int);             /* postfix */
  constexpr File operator+() const; /* unary */
  constexpr File operator-(const Delta& rha) const;
  constexpr Delta operator-(const File& rha) const;
  File& operator-=(const Delta& rha);
  File& operator--();   /* prefix */
  File operator--(int); /* postfix */

  /* there is no unary minus as there are no negative Files */
  /* there are no multiply or divides or mods.  how do you multiply a File */
  /** @brief Convert a File to a char.
   * @details The leftmost File to 'a', ..., the rightmost File to 'h'.
   * other values may throw an exception or, more likely, just return nonse.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr explicit operator char() const;
  /** @brief Test that a File is valid (on the board).
   * @details If it is, true.  If not, false.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr void* is_valid() const;
  /** @brief Test that a char is a valid file.
   * @details If it is, true.  If not, false.
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr void* is_valid(char);
  /** @brief synonym for File::is_valid().
   * @details If it is, true.  If not, false.
   * void * implicit converts to bool, but unlike bool does not implicit convert
   * to int
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr operator void*() const;
  /** @brief Return false if the File is valid, and true if it is not.
   * void * implicit converts to bool, but unlike bool does not implicit convert
   * to int
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr void* operator!() const;

  /* these are required or these operations don't work; there is no default */
  constexpr void* operator==(const File& rha) const;
  constexpr void* operator!=(const File& rha) const;

  /** @brief Return a bitboard_t mask with 1s in this File and 0s in all others.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr bitboard_t bitboard() const;

 private:
  explicit constexpr File(int);
  index_t value;
};

class File::Delta {
 public:
  friend class File;
  /** @brief Construct a Delta of zero.
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr Delta zero();

  /** @brief Construct a Delta moving one right on the board (white's POV)
   * @details combine with operator* for larger deltas
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr Delta right();

  /** @brief Construct a Delta moving one left on the board (white's POV)
   * @details combine with operator* for larger deltas
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr Delta left();

  /* to avoid clutter operator doc comments go in File.cpp */
  constexpr Delta operator+(const Delta& rha) const;
  constexpr File operator+(const File& rha) const;
  Delta& operator+=(const Delta& rha);
  Delta& operator++();               /* prefix */
  Delta operator++(int);             /* postfix */
  constexpr Delta operator+() const; /* unary */

  constexpr Delta operator-(const Delta& rha) const;
  Delta& operator-=(const Delta& rha);
  Delta& operator--();               /* prefix */
  Delta operator--(int);             /* postfix */
  constexpr Delta operator-() const; /* unary */

  constexpr Delta operator*(int rha) const;
  Delta& operator*=(int rha);
  friend constexpr Delta operator*(int lha, const Delta& rha);

  /* for all div and mod, c++ rules apply for handling negatives */
  constexpr Delta operator/(int rha) const;
  constexpr int operator/(const Delta& rha) const;
  Delta& operator/=(int rha);
  constexpr Delta operator%(const int& rha) const;
  constexpr Delta operator%(const Delta& rha) const;
  Delta& operator%=(const int& rha);
  Delta& operator%=(const Delta& rha);
  /* next 3 funcs:  void * implicit converts to bool, but unlike bool does not
   * implicit convert to int */
  constexpr operator void*() const;
  /* just to keep !deltavar from returning bool and implicit convert to int */
  constexpr void* operator!() const;
  constexpr void* operator==(const Delta& rha) const;
  constexpr void* operator!=(const Delta& rha) const;

 private:
  constexpr Delta(); /* zero */
  explicit constexpr Delta(int);
  File::index_t value;
};

}  // namespace Chess
}  // namespace JMHB

#endif /* ifndef JMHB_CHESS_FILE_HPP */
