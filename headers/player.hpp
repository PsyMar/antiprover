// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for the Player class.
 * @version Antiprover 0.0.2
 * @date 2021-01-17
 * @since Antiprover 0.0.1 2021-01-14
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file.
 */

#ifndef JMHB_CHESS_PLAYER_HPP_
/** @brief player.hpp header define wrapper
 * @since Antiprover 0.0.2 2021-01-14 */
#define JMHB_CHESS_PLAYER_HPP_

namespace JMHB {
namespace Chess {

class Piece;
class PieceType;

/** @brief A class for the two colors of chess piece, and the players
 * controlling them.
 * @details Even an enum seems too much, but this way we can OR it with pieces.
 * @since Antiprover 0.0.2 2021-01-13 */
class Player {
 public:
  Player() = delete; /* there is no default constructor */

  /** @brief goes first, might play e4
   * @since Antiprover 0.0.2 2021-01-13 */
  static constexpr Player WHITE();

  /** @brief goes second, might respond e5 to e4
   * @since Antiprover 0.0.2 2021-01-13 */
  static constexpr Player BLACK();

  /** @brief Get a character representing who is to move.
   * @details 'w' for white, 'b' for black.
   * @since Antiprover 0.0.2 2021-01-14 */
  explicit constexpr operator char() const;

  /** @brief Convert from a char.
   * @see Player::operator char()
   * @since Antiprover 0.0.2 2021-01-16 */
  explicit constexpr Player(char);

  /** @brief Test equality; void* implicit converts to bool but not int.
   * @since Antiprover 0.0.2 2021-01-14 */
  constexpr bool operator==(const Player& rha) const;

  /** @brief Test inequality; void* implicit converts to bool but not int.
   * @since Antiprover 0.0.2 2021-01-14 */
  constexpr bool operator!=(const Player& rha) const;

  /** @brief If white, get black; if black, get white.
   * @since Antiprover 0.0.2 2021-01-13 */
  constexpr Player operator~() const;

  /** @brief Combine a Player with a PieceType to get a Piece.
   * @details Operator& and Operator+ are synonymous for these classes.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Piece operator&(const PieceType& rha) const;

  /** @brief Combine a Player with a PieceType to get a Piece.
   * @details Operator& and Operator+ are synonymous for these classes.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Piece operator+(const PieceType& rha) const;

 private:
  constexpr Player(bool);
  bool is_black;
}; /* class Player */

}  // namespace Chess
}  // namespace JMHB

#endif /* JMHB_CHESS_PLAYER_HPP_ */
