// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for the Square class.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.1 2021-01-04
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_CHESS_SQUARE_HPP_
/** @brief square.hpp define header
 * @since Antiprover 0.0.2 2021-01-22 */
#define JMHB_CHESS_SQUARE_HPP_

#include <string>

#include "file.hpp"
#include "rank.hpp"
#include "types.hpp"

namespace JMHB {
namespace Chess {

/** @brief A class for squares.
 * @since Antiprover 0.0.2 2021-01-13 */
class Square {
 public:
  /* NO DEFAULT CONSTRUCTOR */
  Square() = delete;

  /** @brief type used as an index for ranks
   * @since Antiprover 0.0.2 2021-01-23 */
  using index_t = int;

  /** @brief Square - Square -> Square::Delta
   * @details two-dimensional vector with rank and file differences.
   * @note defines way more operators than are realistically gonna be used.
   * @since Antiprover 0.0.2 2021-01-23 */
  class Delta;

  /** @brief The number of squares on the board.
   * @since Antiprover 0.0.2 2021-01-23 */
  static constexpr index_t NUM_SQUARES = File::NUM_FILES * Rank::NUM_RANKS;
  /** @brief Construct a Square from an existing Rank and File.
   * @since Antiprover 0.0.2 2021-01-23 */
  constexpr Square(const Rank &, const File &);
  /** @brief Construct a Square from an existing File and Rank.
   * @since Antiprover 0.0.2 2021-01-23 */
  constexpr Square(const File &, const Rank &);
  /** @brief Construct a Square from a string, such as "a1" or "e4".
   * @details First character must be file, second must be rank.
   * Files are 'a' through 'h' and ranks are '1' through '8'.
   * @since Antiprover 0.0.2 2021-01-23 */
  Square(const std::string &);

  /** @brief Construct a square from the least significant set bit of the
   * bitboard.
   * @details Argument must be nonzero (i.e. have at least one bit set).
   * The index of the bit (0..63) indicates the square, with the least
   * significant 3 bits being the index (0-7) of the rank ('1' to '8'), and the
   * more significant 3 bits being the index (0-7) of the file ('a' to 'h').
   * @since Antiprover 0.0.2 2021-01-23 */
  static constexpr Square low_one(const bitboard_t &);
  /** @brief Construct a square from the least significant set bit of the
   * bitboard, and unset that bit.
   * @details Argument must be nonzero (i.e. have at least one bit set).
   * The index of the bit (0..63) indicates the square, with the least
   * significant 3 bits being the index (0-7) of the rank ('1' to '8'), and the
   * more significant 3 bits being the index (0-7) of the file ('a' to 'h').
   * @since Antiprover 0.0.2 2021-01-23 */
  static constexpr Square pop_low_one(bitboard_t &);

  /** @brief Create a square from an index as used for arrays or bitboards.
   * @details Indices range from 0 to 63; outside this range is invalid and
   * *may* throw an exception. Honestly I doubt I will use this over pop_low_one
   * ever, but just in case, it's here. Certainly it's not as fast this way.
   * @since Antiprover 0.0.2 2021-01-23 */
  static constexpr Square from_index();
  /** @brief Convert a Square to an index for indexing arrays or bitboards.
   * @details Indices range from 0 to 63 for valid squares, and outside this
   * range for invalid.
   * @since Antiprover 0.0.2 2021-01-23 */
  constexpr index_t to_index() const;
  /** @brief Get the Rank a square is on.
   * @since Antiprover 0.0.2 2021-01-23 */
  constexpr Rank rank() const;
  /** @brief Get the File a square is on.
   * @since Antiprover 0.0.2 2021-01-23 */
  constexpr File file() const;
  /** @brief Convert a square to a bitboard with a 1-bit set for that square and
   * the other bits 0.
   * @details The square must be a valid, on-the-board square.
   * @since Antiprover 0.0.2 2021-01-23 */
  constexpr bitboard_t bitboard() const;
  /** @brief Get the standard algebraic representation of a square.
   * @details The square must be a valid, on-the-board square.
   * The string will be two characters -- a file from 'a' to 'h' and a rank from
   * '1' to '8'.
   * @since Antiprover 0.0.2 2021-01-23 */
  constexpr std::string to_string() const;

  constexpr Square operator+(const Delta &rha) const;
  constexpr Square operator+(const Rank::Delta &rha) const;
  constexpr Square operator+(const File::Delta &rha) const;
  /* unary plus and increment not defined */
  Square &operator+=(const Delta &rha);
  Square &operator+=(const Rank::Delta &rha);
  Square &operator+=(const File::Delta &rha);
  constexpr Delta operator-(const Square &rha) const;
  constexpr Square operator-(const Delta &rha) const;
  constexpr Square operator-(const Rank::Delta &rha) const;
  constexpr Square operator-(const File::Delta &rha) const;
  /* unary minus and decrement not defined */
  Square &operator-=(const Delta &rha);
  Square &operator-=(const Rank::Delta &rha);
  Square &operator-=(const File::Delta &rha);

  /* conversion to void* is a synonym for is_valid(). */
  constexpr operator void *() const;
  constexpr void *operator!() const;

  constexpr void *operator==(const Square &rha) const;
  constexpr void *operator!=(const Square &rha) const;

  class Delta {
   public:
    constexpr Delta(); /* zero */
    constexpr Delta(const Rank::Delta &);
    constexpr Delta(const File::Delta &);
    constexpr Delta(const Rank::Delta &, const File::Delta &);
    constexpr Delta(const File::Delta &, const Rank::Delta &);
    constexpr Delta zero();
    constexpr Delta up();
    constexpr Delta down();
    constexpr Delta left();
    constexpr Delta right();
    constexpr File::Delta files() const;
    constexpr Rank::Delta ranks() const;
    constexpr Square operator+(const Square &rha) const;
    constexpr Delta operator+(const Delta &rha) const;
    constexpr Delta operator+(const Rank::Delta &rha) const;
    constexpr Delta operator+(const File::Delta &rha) const;
    constexpr Delta operator-(const Delta &rha) const;
    constexpr Delta operator-(const Rank::Delta &rha) const;
    constexpr Delta operator-(const File::Delta &rha) const;
    Delta &operator+=(const Delta &rha);
    Delta &operator+=(const Rank::Delta &rha);
    Delta &operator+=(const File::Delta &rha);
    Delta &operator-=(const Delta &rha);
    Delta &operator-=(const Rank::Delta &rha);
    Delta &operator-=(const File::Delta &rha);
    constexpr Delta operator+() const; /* unary */
    constexpr Delta operator-() const; /* unary */
    constexpr operator void *() const;
    constexpr void *operator!() const;
    constexpr double magnitude() const;
    constexpr index_t magnitude_squared() const;

   private:
    index_t index_d;
    Rank::Delta rank_d;
    File::Delta file_d;
  }; /* end delta, back to Square */

 private:
  constexpr Square(index_t);
  index_t my_index;
  /* only ever used if my_index is out of the valid range */
  Rank my_rank;
  File my_file;
};

constexpr Square operator&(const Rank &, const File &);
constexpr Square operator&(const File &, const Rank &);

}  // namespace Chess
}  // namespace JMHB

#endif /* ifndef JMHB_CHESS_SQUARE_HPP_ */
