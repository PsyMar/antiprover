// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for the Rank class.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.2 2021-01-17
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_CHESS_RANK_HPP_
/** @brief rank.hpp header define wrapper
 * @since Antiprover 0.0.2 2021-01-19 */
#define JMHB_CHESS_RANK_HPP_

#include "types.hpp"

namespace JMHB {
namespace Chess {

class Square;

/** @brief A class for ranks of a chessboard.
 * @sa File
 * @sa Square
 * @since Antiprover 0.0.2 2021-01-19 */
class Rank {
 public:
  /** @brief type used as an index for ranks
   * @since Antiprover 0.0.2 2021-01-19 */
  using index_t = int;

  /** @brief Rank - Rank -> Rank::Delta
   * @details Positive value -> up (white's POV)
   * Negative -> down (white's POV)
   * Zero -> no change
   * @note defines way more operators than are realistically gonna be used.
   * @since Antiprover 0.0.2 2021-01-19 */
  class Delta;

  /* NO default constructor */
  Rank() = delete;

  /** @brief The number of ranks on a chessboard.
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr index_t NUM_RANKS = 8;
  /** @brief Convert a character to a Rank.
   * @details '1' -> the first rank, '2' -> the second rank, up to '8' -> the
   * eighth rank. Other characters may throw an invalid_argument exception.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr Rank(char);

  /* to avoid clutter operator doc comments go in Rank.cpp */
  constexpr Rank operator+(const Delta& rha) const;
  Rank& operator+=(const Delta& rha);
  Rank& operator++();               /* prefix */
  Rank operator++(int);             /* postfix */
  constexpr Rank operator+() const; /* unary */
  constexpr Rank operator-(const Delta& rha) const;
  constexpr Delta operator-(const Rank& rha) const;
  Rank& operator-=(const Delta& rha);
  Rank& operator--();   /* prefix */
  Rank operator--(int); /* postfix */

  /* there is no unary minus as there are no negative ranks */
  /* there are no multiply or divides or mods.  how do you multiply a rank */
  /** @brief Convert a Rank to a char.
   * @details The first rank to '1', ..., the eighth rank to '8'.
   * other values may throw an exception or, more likely, just return nonse.
   * Check if Rank::is_valid() first.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr explicit operator char() const;
  /** @brief Test that a rank is valid (on the board).
   * @details If it is, true.  If not, false.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr void* is_valid() const;
  /** @brief Test that a char is a valid rank.
   * @details If it is, true.  If not, false.
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr void* is_valid(char);
  /** @brief synonym for Rank::is_valid().
   * @details If it is, true.  If not, false.
   * void * implicit converts to bool, but unlike bool does not implicit convert
   * to int
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr operator void*() const;
  /** @brief Return false if the rank is valid, and true if it is not.
   * void * implicit converts to bool, but unlike bool does not implicit convert
   * to int
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr void* operator!() const;

  /* these are required or these operations don't work; there is no default */
  constexpr void* operator==(const Rank& rha) const;
  constexpr void* operator!=(const Rank& rha) const;

  /** @brief Return a bitboard_t mask with 1s in this Rank and 0s in all others.
   * @since Antiprover 0.0.2 2021-01-19 */
  constexpr bitboard_t bitboard() const;

 private:
  explicit constexpr Rank(index_t);
  index_t value;
};

class Rank::Delta {
 public:
  friend class Rank;

  /** @brief Construct a Delta of zero.
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr Delta zero();

  /** @brief Construct a Delta moving one up the board (white's POV)
   * @details combine with operator* for larger deltas
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr Delta up();

  /** @brief Construct a Delta moving one down the board (white's POV)
   * @details combine with operator* for larger deltas
   * @since Antiprover 0.0.2 2021-01-19 */
  static constexpr Delta down();

  /* to avoid clutter operator doc comments go in Rank.cpp */
  constexpr Delta operator+(const Delta& rha) const;
  constexpr Rank operator+(const Rank& rha) const;
  Delta& operator+=(const Delta& rha);
  Delta& operator++();               /* prefix */
  Delta operator++(int);             /* postfix */
  constexpr Delta operator+() const; /* unary */

  constexpr Delta operator-(const Delta& rha) const;
  Delta& operator-=(const Delta& rha);
  Delta& operator--();               /* prefix */
  Delta operator--(int);             /* postfix */
  constexpr Delta operator-() const; /* unary */

  constexpr Delta operator*(int rha) const;
  Delta& operator*=(int rha);
  friend constexpr Delta operator*(int lha, const Delta& rha);

  /* for all div and mod, c++ rules apply for handling negatives */
  constexpr Delta operator/(int rha) const;
  constexpr int operator/(const Delta& rha) const;
  Delta& operator/=(int rha);
  constexpr Delta operator%(const int& rha) const;
  constexpr Delta operator%(const Delta& rha) const;
  Delta& operator%=(const int& rha);
  Delta& operator%=(const Delta& rha);
  /* next 3 funcs:  void * implicit converts to bool, but unlike bool does not
   * implicit convert to int */
  constexpr operator void*() const;
  constexpr void* operator!()
      const; /* just to keep !deltavar from returning bool */
  constexpr void* operator==(const Delta& rha) const;
  constexpr void* operator!=(const Delta& rha) const;

 private:
  constexpr Delta(); /* zero */
  explicit constexpr Delta(int);
  Rank::index_t value;
};

}  // namespace Chess
}  // namespace JMHB
#endif /* ifndef JMHB_CHESS_RANK_HPP_ */
