#if 0
// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for constants regarding the chessboard itself.
 * @version Antiprover 0.0.1
 * @date 2021-01-27
 * @since Antiprover 0.0.1 2020-12-31
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef BOARD_CONSTANTS_H_
/** @brief board_constants.h header define wrapper
 * @since Antiprover 0.0.1 2020-12-31 */
#define BOARD_CONSTANTS_H_

#include <array>
#include <cassert>
#include <string>

#include "0compile_time_options.hpp"  // for ALLOW_DEBUG/NDEBUG
#include "square.hpp"
#include "types.hpp"

/** @brief Convert rank and file to square.
 * @since Antiprover 0.0.1 2020-12-31
 */
constexpr Square to_square(Rank ra, File fi) {
  return static_cast<Square>((static_cast<square_t>(fi) << 3) |
                             (static_cast<square_t>(ra)));
};

/** @brief Number of diagonals on a chessboard.
 * @details okay, this is a bit more eclectic
 * @since Antiprover 0.0.1 2020-12-31
 */
constexpr auto NUM_DIAGS = Rank::NUM_RANKS + File::NUM_FILES - 1;

static_assert(NUM_DIAGS == 15);

/** @brief An enum for diagonals that go from bottom-left up to top-right.
 * @details Several functions *ARE* sensitive to or based on the order here,
 * don't mess with it!
 * @since Antiprover 0.0.1 2020-12-31
 */
enum struct Diag_up : square_t {
  a8a8 = 0,
  a7b8 = 1,
  a6c8 = 2,
  a5d8 = 3,
  a4e8 = 4,
  a3f8 = 5,
  a2g8 = 6,
  a1h8 = 7,
  b1h7 = 8,
  c1h6 = 9,
  d1h5 = 10,
  e1h4 = 11,
  f1h3 = 12,
  g1h2 = 13,
  h1h1 = 14
};

/** @brief Get the upwards-sloping diagonal on which a square lies.
 * @details upwards-sloping diagonals are parallel to the dark-squared main
 * diagonal a1h8.
 * @since Antiprover 0.0.1 2020-12-31
 */
constexpr Diag_up get_diag_up(Square sq) {
  return static_cast<Diag_up>(static_cast<square_t>(7) -
                              static_cast<square_t>(get_rank(sq)) +
                              static_cast<square_t>(get_file(sq)));
}

/** @brief An enum for diagonals that go from top-left down to bottom-right.
 * @details Several functions *ARE* sensitive to or based on the order here,
 * don't mess with it!
 * @since Antiprover 0.0.1 2020-12-31
 */
enum struct Diag_down : square_t {
  a1a1 = 0,
  a2b1 = 1,
  a3c1 = 2,
  a4d1 = 3,
  a5e1 = 4,
  a6f1 = 5,
  a7g1 = 6,
  a8h1 = 7,
  b8h2 = 8,
  c8h3 = 9,
  d8h4 = 10,
  e8h5 = 11,
  f8h6 = 12,
  g8h7 = 13,
  h8h8 = 14
};

/** @brief Get the downwards-sloping diagonal on which a square lies.
 * @details downwards-sloping diagonals are parallel to the light-squared main
 * diagonal a8h1.
 * @since Antiprover 0.0.1 2020-12-31
 */
constexpr Diag_down get_diag_down(Square sq) {
  return static_cast<Diag_down>(static_cast<square_t>(get_rank(sq)) +
                                static_cast<square_t>(get_file(sq)));
}

/** @brief A bitboard representing the squares of the dark-squared main
 * diagonal.
 * @since Antiprover 0.0.1 2020-12-31
 */                                       /*1234567812345678*/
constexpr bitboard_t DIAG_A1H8 = UINT64_C(0x8040201008040201);

/** @brief A bitboard representing the squares of the dark-squared main
 * diagonal.
 * @since Antiprover 0.0.1 2020-12-31
 */                                       /*1234567812345678*/
constexpr bitboard_t DIAG_A8H1 = UINT64_C(0x0102040810204080);

/** @brief An array of bitboards representing the squares of each up-diagonal.
 * @since Antiprover 0.0.1 2020-12-31
 */ // operator precedence level 13 (*) comes before 11 (<<)
    // https://en.cppreference.com/w/cpp/language/operator_precedence
constexpr std::array<bitboard_t, NUM_DIAGS> DIAG_UP_BOARDS = {
    /* oops, the way we defined these, they go in reverse order
    but our other functions are already written that way so */
    DIAG_A1H8 >> Board::NUM_RANKS * 7, DIAG_A1H8 >> Board::NUM_RANKS * 6,
    DIAG_A1H8 >> Board::NUM_RANKS * 5, DIAG_A1H8 >> Board::NUM_RANKS * 4,
    DIAG_A1H8 >> Board::NUM_RANKS * 3, DIAG_A1H8 >> Board::NUM_RANKS * 2,
    DIAG_A1H8 >> Board::NUM_RANKS * 1, DIAG_A1H8,
    DIAG_A1H8 << Board::NUM_RANKS * 1, DIAG_A1H8 << Board::NUM_RANKS * 2,
    DIAG_A1H8 << Board::NUM_RANKS * 3, DIAG_A1H8 << Board::NUM_RANKS * 4,
    DIAG_A1H8 << Board::NUM_RANKS * 5, DIAG_A1H8 << Board::NUM_RANKS * 6,
    DIAG_A1H8 << Board::NUM_RANKS * 7};

/** @brief An array of bitboards representing the squares of each down-diagonal.
 * @since Antiprover 0.0.1 2020-12-31
 */ // operator precedence level 13 (*) comes before 11 (<<)
    // https://en.cppreference.com/w/cpp/language/operator_precedence
constexpr std::array<bitboard_t, NUM_DIAGS> DIAG_DOWN_BOARDS = {
    DIAG_A8H1 << Board::NUM_RANKS * 7, DIAG_A8H1 << Board::NUM_RANKS * 6,
    DIAG_A8H1 << Board::NUM_RANKS * 5, DIAG_A8H1 << Board::NUM_RANKS * 4,
    DIAG_A8H1 << Board::NUM_RANKS * 3, DIAG_A8H1 << Board::NUM_RANKS * 2,
    DIAG_A8H1 << Board::NUM_RANKS * 1, DIAG_A8H1,
    DIAG_A8H1 >> Board::NUM_RANKS * 1, DIAG_A8H1 >> Board::NUM_RANKS * 2,
    DIAG_A8H1 >> Board::NUM_RANKS * 3, DIAG_A8H1 >> Board::NUM_RANKS * 4,
    DIAG_A8H1 >> Board::NUM_RANKS * 5, DIAG_A8H1 >> Board::NUM_RANKS * 6,
    DIAG_A8H1 >> Board::NUM_RANKS * 7};

/** @brief Convert an up-diagonal to a bitboard of the squares in that diagonal.
 * @since Antiprover 0.0.1 2020-12-31
 */
constexpr auto to_board(Diag_up du) {
  return DIAG_UP_BOARDS[static_cast<square_t>(du)];
}

/** @brief Convert a down-diagonal to a bitboard of the squares in that
 * diagonal.
 * @since Antiprover 0.0.1 2020-12-31
 */
constexpr auto to_board(Diag_down dd) {
  return DIAG_DOWN_BOARDS[static_cast<square_t>(dd)];
}

/** @brief An array of square adjacency bitboards.
 * @details 1 where the square in question is adjacent (not equal) to the start
 * square. Other than castling this is how the king moves. Python code to
 * generate this array's data:
 *
 * adjacency = [0]*64
 * for file in range(8):
 *     for rank in range(8):
 *         for fileoff in [file-1, file, file+1]:
 *             for rankoff in [rank-1, rank, rank+1]:
 *                 if (fileoff,rankoff) != (file, rank):
 *                     if fileoff in range(8) and rankoff in range(8):
 *                         adjacency[file*8+rank] += (1 << (fileoff*8+rankoff))
 *
 * for z,i in enumerate(adjacency):
 *     num = "%064d" % int(bin(i)[2:])
 *     num2 = ""
 *     for q in range(8):
 *         num2 = num2 + num[q*8:q*8+8] + "'"
 *     num2 = num2[:-1]
 *     print("    static_cast<bitboard_t>(UINT64_C(0b" + num2 + ")), //" +
 * str(z))
 *
 * @since Antiprover 0.0.1 2021-01-01
 */ /* constexpr doesn't make the code too bulky because this is only used in one function, get_adjacent */
constexpr std::array<bitboard_t, Board::NUM_SQUARES> ADJACENCY_ARRAY = {
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'00000011'00000010)),  // 0
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'00000111'00000101)),  // 1
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'00001110'00001010)),  // 2
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'00011100'00010100)),  // 3
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'00111000'00101000)),  // 4
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'01110000'01010000)),  // 5
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'11100000'10100000)),  // 6
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000000'11000000'01000000)),  // 7
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000011'00000010'00000011)),  // 8
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000111'00000101'00000111)),  // 9
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00001110'00001010'00001110)),  // 10
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00011100'00010100'00011100)),  // 11
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00111000'00101000'00111000)),  // 12
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'01110000'01010000'01110000)),  // 13
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'11100000'10100000'11100000)),  // 14
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'11000000'01000000'11000000)),  // 15
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000011'00000010'00000011'00000000)),  // 16
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000111'00000101'00000111'00000000)),  // 17
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00001110'00001010'00001110'00000000)),  // 18
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00011100'00010100'00011100'00000000)),  // 19
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00111000'00101000'00111000'00000000)),  // 20
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'01110000'01010000'01110000'00000000)),  // 21
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'11100000'10100000'11100000'00000000)),  // 22
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'11000000'01000000'11000000'00000000)),  // 23
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000011'00000010'00000011'00000000'00000000)),  // 24
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000111'00000101'00000111'00000000'00000000)),  // 25
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00001110'00001010'00001110'00000000'00000000)),  // 26
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00011100'00010100'00011100'00000000'00000000)),  // 27
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00111000'00101000'00111000'00000000'00000000)),  // 28
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'01110000'01010000'01110000'00000000'00000000)),  // 29
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'11100000'10100000'11100000'00000000'00000000)),  // 30
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'11000000'01000000'11000000'00000000'00000000)),  // 31
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000011'00000010'00000011'00000000'00000000'00000000)),  // 32
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000111'00000101'00000111'00000000'00000000'00000000)),  // 33
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00001110'00001010'00001110'00000000'00000000'00000000)),  // 34
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00011100'00010100'00011100'00000000'00000000'00000000)),  // 35
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00111000'00101000'00111000'00000000'00000000'00000000)),  // 36
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'01110000'01010000'01110000'00000000'00000000'00000000)),  // 37
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'11100000'10100000'11100000'00000000'00000000'00000000)),  // 38
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'11000000'01000000'11000000'00000000'00000000'00000000)),  // 39
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000011'00000010'00000011'00000000'00000000'00000000'00000000)),  // 40
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000111'00000101'00000111'00000000'00000000'00000000'00000000)),  // 41
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00001110'00001010'00001110'00000000'00000000'00000000'00000000)),  // 42
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00011100'00010100'00011100'00000000'00000000'00000000'00000000)),  // 43
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00111000'00101000'00111000'00000000'00000000'00000000'00000000)),  // 44
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'01110000'01010000'01110000'00000000'00000000'00000000'00000000)),  // 45
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'11100000'10100000'11100000'00000000'00000000'00000000'00000000)),  // 46
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'11000000'01000000'11000000'00000000'00000000'00000000'00000000)),  // 47
    static_cast<bitboard_t>(UINT64_C(
        0b00000011'00000010'00000011'00000000'00000000'00000000'00000000'00000000)),  // 48
    static_cast<bitboard_t>(UINT64_C(
        0b00000111'00000101'00000111'00000000'00000000'00000000'00000000'00000000)),  // 49
    static_cast<bitboard_t>(UINT64_C(
        0b00001110'00001010'00001110'00000000'00000000'00000000'00000000'00000000)),  // 50
    static_cast<bitboard_t>(UINT64_C(
        0b00011100'00010100'00011100'00000000'00000000'00000000'00000000'00000000)),  // 51
    static_cast<bitboard_t>(UINT64_C(
        0b00111000'00101000'00111000'00000000'00000000'00000000'00000000'00000000)),  // 52
    static_cast<bitboard_t>(UINT64_C(
        0b01110000'01010000'01110000'00000000'00000000'00000000'00000000'00000000)),  // 53
    static_cast<bitboard_t>(UINT64_C(
        0b11100000'10100000'11100000'00000000'00000000'00000000'00000000'00000000)),  // 54
    static_cast<bitboard_t>(UINT64_C(
        0b11000000'01000000'11000000'00000000'00000000'00000000'00000000'00000000)),  // 55
    static_cast<bitboard_t>(UINT64_C(
        0b00000010'00000011'00000000'00000000'00000000'00000000'00000000'00000000)),  // 56
    static_cast<bitboard_t>(UINT64_C(
        0b00000101'00000111'00000000'00000000'00000000'00000000'00000000'00000000)),  // 57
    static_cast<bitboard_t>(UINT64_C(
        0b00001010'00001110'00000000'00000000'00000000'00000000'00000000'00000000)),  // 58
    static_cast<bitboard_t>(UINT64_C(
        0b00010100'00011100'00000000'00000000'00000000'00000000'00000000'00000000)),  // 59
    static_cast<bitboard_t>(UINT64_C(
        0b00101000'00111000'00000000'00000000'00000000'00000000'00000000'00000000)),  // 60
    static_cast<bitboard_t>(UINT64_C(
        0b01010000'01110000'00000000'00000000'00000000'00000000'00000000'00000000)),  // 61
    static_cast<bitboard_t>(UINT64_C(
        0b10100000'11100000'00000000'00000000'00000000'00000000'00000000'00000000)),  // 62
    static_cast<bitboard_t>(UINT64_C(
        0b01000000'11000000'00000000'00000000'00000000'00000000'00000000'00000000))  // 63
};

/** @brief Given a Square, get the adjacent squares' bits.
 * @since Antiprover 0.0.1 2021-01-01
 */ /* TODO: write unit tests.  somehow */
constexpr bitboard_t get_adjacent(Square sq) {
  return ADJACENCY_ARRAY[static_cast<square_t>(sq)];
}

/** @brief An array of knight-move bitboards.
 * @details 1 where the square in question is a knight's move from the start
 * square. Python code to generate this array's data:
 *
 * knights = [0]*64
 * for file in range(8):
 *     for rank in range(8):
 *         movesarray = [(file+f, rank+r) for (f, r) in zip([-2, -2, -1, -1, 1,
 * 1, 2, 2],[-1, 1, -2, 2, -2, 2, -1, 1])] for fileoff, rankoff in movesarray:
 *             if (fileoff,rankoff) != (file, rank):
 *                 if fileoff in range(8) and rankoff in range(8):
 *                     knights[file*8+rank] += (1 << (fileoff*8+rankoff))
 *
 * for z,i in enumerate(knights):
 *     num = "%064d" % int(bin(i)[2:])
 *     num2 = ""
 *     for q in range(8):
 *         num2 = num2 + num[q*8:q*8+8] + "'"
 *     num2 = num2[:-1]
 *     print("    static_cast<bitboard_t>(UINT64_C(0b" + num2 + ")), //" +
 * str(z))
 *
 * @since Antiprover 0.0.1 2021-01-01
 */ /* constexpr doesn't make the code too bulky because this array is only used in get_knight_moves */
constexpr std::array<bitboard_t, Board::NUM_SQUARES> KNIGHT_MOVE_ARRAY = {
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000010'00000100'00000000)),  // 0
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00000101'00001000'00000000)),  // 1
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00001010'00010001'00000000)),  // 2
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00010100'00100010'00000000)),  // 3
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'00101000'01000100'00000000)),  // 4
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'01010000'10001000'00000000)),  // 5
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'10100000'00010000'00000000)),  // 6
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000000'01000000'00100000'00000000)),  // 7
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000010'00000100'00000000'00000100)),  // 8
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00000101'00001000'00000000'00001000)),  // 9
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00001010'00010001'00000000'00010001)),  // 10
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00010100'00100010'00000000'00100010)),  // 11
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'00101000'01000100'00000000'01000100)),  // 12
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'01010000'10001000'00000000'10001000)),  // 13
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'10100000'00010000'00000000'00010000)),  // 14
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000000'01000000'00100000'00000000'00100000)),  // 15
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000010'00000100'00000000'00000100'00000010)),  // 16
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00000101'00001000'00000000'00001000'00000101)),  // 17
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00001010'00010001'00000000'00010001'00001010)),  // 18
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00010100'00100010'00000000'00100010'00010100)),  // 19
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'00101000'01000100'00000000'01000100'00101000)),  // 20
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'01010000'10001000'00000000'10001000'01010000)),  // 21
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'10100000'00010000'00000000'00010000'10100000)),  // 22
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000000'01000000'00100000'00000000'00100000'01000000)),  // 23
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000010'00000100'00000000'00000100'00000010'00000000)),  // 24
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00000101'00001000'00000000'00001000'00000101'00000000)),  // 25
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00001010'00010001'00000000'00010001'00001010'00000000)),  // 26
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00010100'00100010'00000000'00100010'00010100'00000000)),  // 27
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'00101000'01000100'00000000'01000100'00101000'00000000)),  // 28
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'01010000'10001000'00000000'10001000'01010000'00000000)),  // 29
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'10100000'00010000'00000000'00010000'10100000'00000000)),  // 30
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000000'01000000'00100000'00000000'00100000'01000000'00000000)),  // 31
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000010'00000100'00000000'00000100'00000010'00000000'00000000)),  // 32
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000101'00001000'00000000'00001000'00000101'00000000'00000000)),  // 33
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00001010'00010001'00000000'00010001'00001010'00000000'00000000)),  // 34
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00010100'00100010'00000000'00100010'00010100'00000000'00000000)),  // 35
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00101000'01000100'00000000'01000100'00101000'00000000'00000000)),  // 36
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'01010000'10001000'00000000'10001000'01010000'00000000'00000000)),  // 37
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'10100000'00010000'00000000'00010000'10100000'00000000'00000000)),  // 38
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'01000000'00100000'00000000'00100000'01000000'00000000'00000000)),  // 39
    static_cast<bitboard_t>(UINT64_C(
        0b00000010'00000100'00000000'00000100'00000010'00000000'00000000'00000000)),  // 40
    static_cast<bitboard_t>(UINT64_C(
        0b00000101'00001000'00000000'00001000'00000101'00000000'00000000'00000000)),  // 41
    static_cast<bitboard_t>(UINT64_C(
        0b00001010'00010001'00000000'00010001'00001010'00000000'00000000'00000000)),  // 42
    static_cast<bitboard_t>(UINT64_C(
        0b00010100'00100010'00000000'00100010'00010100'00000000'00000000'00000000)),  // 43
    static_cast<bitboard_t>(UINT64_C(
        0b00101000'01000100'00000000'01000100'00101000'00000000'00000000'00000000)),  // 44
    static_cast<bitboard_t>(UINT64_C(
        0b01010000'10001000'00000000'10001000'01010000'00000000'00000000'00000000)),  // 45
    static_cast<bitboard_t>(UINT64_C(
        0b10100000'00010000'00000000'00010000'10100000'00000000'00000000'00000000)),  // 46
    static_cast<bitboard_t>(UINT64_C(
        0b01000000'00100000'00000000'00100000'01000000'00000000'00000000'00000000)),  // 47
    static_cast<bitboard_t>(UINT64_C(
        0b00000100'00000000'00000100'00000010'00000000'00000000'00000000'00000000)),  // 48
    static_cast<bitboard_t>(UINT64_C(
        0b00001000'00000000'00001000'00000101'00000000'00000000'00000000'00000000)),  // 49
    static_cast<bitboard_t>(UINT64_C(
        0b00010001'00000000'00010001'00001010'00000000'00000000'00000000'00000000)),  // 50
    static_cast<bitboard_t>(UINT64_C(
        0b00100010'00000000'00100010'00010100'00000000'00000000'00000000'00000000)),  // 51
    static_cast<bitboard_t>(UINT64_C(
        0b01000100'00000000'01000100'00101000'00000000'00000000'00000000'00000000)),  // 52
    static_cast<bitboard_t>(UINT64_C(
        0b10001000'00000000'10001000'01010000'00000000'00000000'00000000'00000000)),  // 53
    static_cast<bitboard_t>(UINT64_C(
        0b00010000'00000000'00010000'10100000'00000000'00000000'00000000'00000000)),  // 54
    static_cast<bitboard_t>(UINT64_C(
        0b00100000'00000000'00100000'01000000'00000000'00000000'00000000'00000000)),  // 55
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00000100'00000010'00000000'00000000'00000000'00000000'00000000)),  // 56
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00001000'00000101'00000000'00000000'00000000'00000000'00000000)),  // 57
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00010001'00001010'00000000'00000000'00000000'00000000'00000000)),  // 58
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00100010'00010100'00000000'00000000'00000000'00000000'00000000)),  // 59
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'01000100'00101000'00000000'00000000'00000000'00000000'00000000)),  // 60
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'10001000'01010000'00000000'00000000'00000000'00000000'00000000)),  // 61
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00010000'10100000'00000000'00000000'00000000'00000000'00000000)),  // 62
    static_cast<bitboard_t>(UINT64_C(
        0b00000000'00100000'01000000'00000000'00000000'00000000'00000000'00000000))  // 63
};

/** @brief Given a Square, get the adjacent squares' bits.
 * @since Antiprover 0.0.1 2021-01-01
 */ /* TODO: write unit tests.  somehow */
constexpr bitboard_t get_knight_moves(Square sq) {
  return KNIGHT_MOVE_ARRAY[static_cast<square_t>(sq)];
}

#endif /* BOARD_CONSTANTS_H_ */
#endif
