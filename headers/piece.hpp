// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Header for constants regarding chess pieces.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.2 2021-01-16
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#ifndef JMHB_CHESS_PIECE_HPP_
/** @brief piece.hpp header define wrapper
 * @since Antiprover 0.0.2 2021-01-16 */
#define JMHB_CHESS_PIECE_HPP_

#include "piecetype.hpp"
#include "player.hpp"

namespace JMHB {
namespace Chess {

/** @brief The piece class, basically a namedtuple for piecetype and player.
 * @since Antiprover 0.0.2 2021-01-16 */
class Piece {
 public:
  Piece() = delete;
  /** @brief Construct a piece from a type and the player who owns it.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Piece(const PieceType &, const Player &);

  /** @brief Construct a piece from a type and the player who owns it.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Piece(const Player &, const PieceType &);

  /** @brief Extract the type of this piece.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr PieceType type() const;

  /** @brief Extract the type of this piece.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr operator PieceType() const;

  /** @brief Extract which player owns this piece.
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Player player() const;

  /** @brief Extract which player owns this piece.
   * @since Antiprover 0.0.2 2021-01-16 */
  explicit constexpr operator Player() const;

  /** @brief Get a char which corresponds to this piece in a FEN string.
   * @details FEN is Forsythe-Edwards Notation.  uppercase for white lowercase
   * for black. Will return one of "KBNQPRkbnqpr".
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr char to_FEN_char() const;

  /** @brief Get a char which corresponds to this piece in a filename.
   * @details Uses rot13 for black pieces to avoid filename clashing on windows.
   * Miraculously that works: "bknpqr" -> "oxacde"
   * Otherwise the same as to_FEN_char().
   * @see to_FEN_char()
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr char to_filename_char() const;

  /** @brief Given a char, get the piece.
   * @details uppercase white, lowercase black.  allows rot13 for black.
   * Accords with the pieces as used in Forsythe-Edwards Notation.
   * @see to_FEN_char()
   * @see to_filename_char()
   * @since Antiprover 0.0.2 2021-01-16 */
  constexpr Piece(char);

  /** @brief Compare two Pieces for equality.
   * @since Antiprover 0.0.2 2021-01-17 */
  constexpr bool operator==(const Piece &rha) const;

  /** @brief Compare two Pieces for inequality.
   * @since Antiprover 0.0.2 2021-01-17 */
  constexpr bool operator!=(const Piece &rha) const;

 private:
  PieceType my_type;
  Player my_player;
};

}  // namespace Chess
}  // namespace JMHB

#endif /* JMHB_CHESS_PIECE_HPP_ */
