// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief A file full of tests for Antiprover.
 * @version Antiprover 0.0.2
 * @date 2021-01-17
 * @since Antiprover 0.0.2 2021-01-17
 * @author "JMHB", cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#include "../src/miscutils.cpp"
#include "../src/piece.cpp"
#include "../src/piecetype.cpp"
#include "../src/player.cpp"

namespace JMHB {
namespace Chess {

/* player tests */
static_assert(static_cast<char>(static_cast<Player>('w')) == 'w');
static_assert(static_cast<char>(static_cast<Player>('b')) == 'b');
static_assert(static_cast<Player>(static_cast<char>(Player::WHITE())) ==
              Player::WHITE());
static_assert(static_cast<Player>(static_cast<char>(Player::BLACK())) ==
              Player::BLACK());
static_assert(Player::WHITE() == Player::WHITE());
static_assert(!(Player::WHITE() != Player::WHITE()));
static_assert(Player::BLACK() == Player::BLACK());
static_assert(!(Player::BLACK() != Player::BLACK()));
static_assert(Player::WHITE() != Player::BLACK());
static_assert(!(Player::WHITE() == Player::BLACK()));
static_assert(Player::WHITE() == ~Player::BLACK());
static_assert(Player::BLACK() == ~Player::WHITE());

/* piecetype tests */
static_assert(static_cast<PieceType>(PieceType::PAWN().to_upper_char()) ==
              PieceType::PAWN());
static_assert(static_cast<PieceType>(PieceType::PAWN().to_lower_char()) ==
              PieceType::PAWN());
static_assert(static_cast<PieceType>(PieceType::PAWN().to_rot13_char()) ==
              PieceType::PAWN());
static_assert(static_cast<PieceType>(PieceType::KNIGHT().to_upper_char()) ==
              PieceType::KNIGHT());
static_assert(static_cast<PieceType>(PieceType::KNIGHT().to_lower_char()) ==
              PieceType::KNIGHT());
static_assert(static_cast<PieceType>(PieceType::KNIGHT().to_rot13_char()) ==
              PieceType::KNIGHT());
static_assert(static_cast<PieceType>(PieceType::BISHOP().to_upper_char()) ==
              PieceType::BISHOP());
static_assert(static_cast<PieceType>(PieceType::BISHOP().to_lower_char()) ==
              PieceType::BISHOP());
static_assert(static_cast<PieceType>(PieceType::BISHOP().to_rot13_char()) ==
              PieceType::BISHOP());
static_assert(static_cast<PieceType>(PieceType::ROOK().to_upper_char()) ==
              PieceType::ROOK());
static_assert(static_cast<PieceType>(PieceType::ROOK().to_lower_char()) ==
              PieceType::ROOK());
static_assert(static_cast<PieceType>(PieceType::ROOK().to_rot13_char()) ==
              PieceType::ROOK());
static_assert(static_cast<PieceType>(PieceType::QUEEN().to_upper_char()) ==
              PieceType::QUEEN());
static_assert(static_cast<PieceType>(PieceType::QUEEN().to_lower_char()) ==
              PieceType::QUEEN());
static_assert(static_cast<PieceType>(PieceType::QUEEN().to_rot13_char()) ==
              PieceType::QUEEN());
static_assert(static_cast<PieceType>(PieceType::KING().to_upper_char()) ==
              PieceType::KING());
static_assert(static_cast<PieceType>(PieceType::KING().to_lower_char()) ==
              PieceType::KING());
static_assert(static_cast<PieceType>(PieceType::KING().to_rot13_char()) ==
              PieceType::KING());
/* test I didn't screw up != */
static_assert(!(static_cast<PieceType>(PieceType::KING().to_upper_char()) !=
                PieceType::KING()));
static_assert(static_cast<PieceType>(PieceType::KING().to_upper_char()) !=
              PieceType::QUEEN());

/* test my rot13ing */
static_assert(rot13(PieceType::BISHOP().to_lower_char()) ==
              PieceType::BISHOP().to_rot13_char());
static_assert(rot13(PieceType::PAWN().to_lower_char()) ==
              PieceType::PAWN().to_rot13_char());
static_assert(rot13(PieceType::QUEEN().to_lower_char()) ==
              PieceType::QUEEN().to_rot13_char());
static_assert(rot13(PieceType::KING().to_lower_char()) ==
              PieceType::KING().to_rot13_char());
static_assert(rot13(PieceType::KNIGHT().to_lower_char()) ==
              PieceType::KNIGHT().to_rot13_char());
static_assert(rot13(PieceType::ROOK().to_lower_char()) ==
              PieceType::ROOK().to_rot13_char());

/* test piecetype/player joining.  == comes before & so use parens */
static_assert(static_cast<Piece>('b') ==
              (Player::BLACK() & PieceType::BISHOP()));
static_assert(static_cast<Piece>('Q') == PieceType::QUEEN() + Player::WHITE());
/* there could be 12 of these -- 24 if you test both & and +...
...48 if you test both orders ...let's just keep going */

/* test they aren't always equal */
static_assert((Player::BLACK() & PieceType::BISHOP()) !=
              (Player::BLACK() & PieceType::ROOK()));
static_assert((Player::BLACK() & PieceType::BISHOP()) !=
              (Player::WHITE() & PieceType::ROOK()));
static_assert((Player::BLACK() & PieceType::ROOK()) !=
              (Player::WHITE() & PieceType::ROOK()));

/* test rot13 maps correctly, again */
static_assert(static_cast<Piece>('o') == static_cast<Piece>('b'));
static_assert(static_cast<Piece>('a') == static_cast<Piece>('n'));
static_assert(static_cast<Piece>('x') == static_cast<Piece>('k'));
static_assert(static_cast<Piece>('c') == static_cast<Piece>('p'));
static_assert(static_cast<Piece>('d') == static_cast<Piece>('q'));
static_assert(static_cast<Piece>('e') == static_cast<Piece>('r'));

/* test FEN chars resolve correctly */
static_assert(static_cast<Piece>('b').to_FEN_char() == 'b');
static_assert(static_cast<Piece>('B').to_FEN_char() == 'B');
static_assert(static_cast<Piece>('n').to_FEN_char() == 'n');
static_assert(static_cast<Piece>('N').to_FEN_char() == 'N');
static_assert(static_cast<Piece>('k').to_FEN_char() == 'k');
static_assert(static_cast<Piece>('K').to_FEN_char() == 'K');
static_assert(static_cast<Piece>('q').to_FEN_char() == 'q');
static_assert(static_cast<Piece>('Q').to_FEN_char() == 'Q');
static_assert(static_cast<Piece>('r').to_FEN_char() == 'r');
static_assert(static_cast<Piece>('R').to_FEN_char() == 'R');
static_assert(static_cast<Piece>('p').to_FEN_char() == 'p');
static_assert(static_cast<Piece>('P').to_FEN_char() == 'P');

/* test filename chars */
static_assert(static_cast<Piece>('o').to_filename_char() == 'o');
static_assert(static_cast<Piece>('a').to_filename_char() == 'a');
static_assert(static_cast<Piece>('c').to_filename_char() == 'c');
static_assert(static_cast<Piece>('d').to_filename_char() == 'd');
static_assert(static_cast<Piece>('e').to_filename_char() == 'e');
static_assert(static_cast<Piece>('x').to_filename_char() == 'x');
static_assert(static_cast<Piece>('Q').to_filename_char() == 'Q');
static_assert(static_cast<Piece>('P').to_filename_char() == 'P');
/* okay we already tested the white pieces under to_FEN_char, and I'm doing this
 * manually so just those two*/

/* that's enough tests for now */
}  // namespace Chess
}  // namespace JMHB
