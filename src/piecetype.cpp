// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implementation for the PieceType class.
 * @version Antiprover 0.0.2
 * @date 2021-01-17
 * @since Antiprover 0.0.2 2021-01-16
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#include "../headers/piecetype.hpp"

#include <cassert>
#include <stdexcept>

#include "../headers/piece.hpp"

namespace JMHB {
namespace Chess {

constexpr PieceType PieceType::PAWN() {
  return PieceType(static_cast<index_t>(5));
  /* pawns come last, as even in anti, pawn cannot promote to pawn, so
   * promotions are 0-4 */
}

constexpr PieceType PieceType::BISHOP() {
  return PieceType(static_cast<index_t>(1));
}

constexpr PieceType PieceType::KNIGHT() {
  return PieceType(static_cast<index_t>(0));
} /* given king/pawn last and Q=B|R, this has to be index 0 */

constexpr PieceType PieceType::ROOK() {
  return PieceType(static_cast<index_t>(2));
}

constexpr PieceType PieceType::QUEEN() {
  return PieceType(static_cast<index_t>(3)); /* queen is rook | bishop */
}

constexpr PieceType PieceType::KING() {
  return PieceType(static_cast<index_t>(4));
} /* after other pieces but before pawn, because anti, but not regular chess,
     allows promote to king */

constexpr PieceType::PieceType(char ch) {
  switch (ch) {
    case 'b': /* fallthrough */
    case 'o':
    case 'B':
    case 'O': /* should never happen since only lower-case pieces should be
                 rot'd but */
      *this = BISHOP();
      return;
    case 'a':
    case 'n':
    case 'A': /* should never happen since only lower-case pieces should be
                 rot'd but */
    case 'N':
      *this = KNIGHT();
      return;
    case 'c':
    case 'C': /* should never happen since only lower-case pieces should be
                 rot'd but */
    case 'p': /* dang one letter off from cCCP */
    case 'P':
      *this = PAWN();
      return;
    case 'd':
    case 'D': /* should never happen since only lower-case pieces should be
                 rot'd but */
    case 'q':
    case 'Q':
      *this = QUEEN();
      return;
    case 'e':
    case 'E': /* should never happen since only lower-case pieces should be
                 rot'd but */
    case 'r':
    case 'R':
      *this = ROOK();
      return;
    case 'k':
    case 'K':
    case 'x':
    case 'X': /* should never happen since only lower-case pieces should be
                 rot'd but */
      *this = KING();
      return;
    default:
      throw std::invalid_argument(
          static_cast<std::string>(
              "Invalid argument sent to PieceType (char):  ") +
          ch);
  } /* end megaswitch */
} /* end PieceType(char) */

/** @brief an array of upper-case characters for pieces.
 * @details out here because constexpr functions can't have static data,
 * apparently.
 * @sa PieceType::to_upper_char()
 * @since Antiprover 0.0.2 2021-01-17 */
constexpr const char* const piecechars_upper = "NBRQKP";
constexpr char PieceType::to_upper_char() const {
  return piecechars_upper[index()];
}

/** @brief an array of lower-case characters for pieces.
 * @details out here because constexpr functions can't have static data,
 * apparently.
 * @sa PieceType::to_lower_char()
 * @since Antiprover 0.0.2 2021-01-17 */
constexpr const char* const piecechars_lower = "nbrqkp";
constexpr char PieceType::to_lower_char() const {
  return piecechars_lower[index()];
}

/** @brief an array of rot13'd lower-case characters for pieces.
 * @details out here because constexpr functions can't have static data,
 * apparently.
 * @sa PieceType::to_rot13_lower_char()
 * @since Antiprover 0.0.2 2021-01-17 */
constexpr const char* const piecechars_rot13 = "aoedxc"; /* rot13("NBRQKP") */
constexpr char PieceType::to_rot13_char() const {
  return piecechars_rot13[index()];
}

constexpr bool PieceType::operator==(const PieceType& rha) const {
  return index() == rha.index();
}

constexpr bool PieceType::operator!=(const PieceType& rha) const {
  return index() != rha.index();
}

constexpr PieceType::index_t PieceType::index() const { return my_index; }

constexpr Piece PieceType::operator&(const Player& rha) const {
  return Piece(rha, *this);
}

constexpr Piece PieceType::operator+(const Player& rha) const {
  return Piece(rha, *this);
}

/** @brief Internal constructor for the constexpr static functions.
 * @details constructs a piecetype from an index.
 * @sa PieceType::my_index
 * @since Antiprover 0.0.2 2021-01-16 */
constexpr PieceType::PieceType(index_t index) : my_index(index) {}

/** @var PieceType::my_index
 * @brief Index of the piece as returned by index()
 * @details 0-5 are knight, bishop, rook, queen, king, pawn, in that order
 * @since Antiprover 0.0.2 2021-01-16 */

}  // namespace Chess
}  // namespace JMHB
