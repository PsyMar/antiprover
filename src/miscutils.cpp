// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief implementation for miscellaneous utility functions.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.1 2021-01-19
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file.
 */
#include "../headers/miscutils.hpp"

namespace JMHB {

constexpr char rot13(char ch) {
  if (ch >= 'a' && ch <= 'z') {
    return 'a' + (((ch - 'a') + 13) % 26);
  } else if (ch >= 'A' && ch <= 'Z') {
    return 'A' + (((ch - 'A') + 13) % 26);
  } else {
    return ch;
  }
}

const void* VOID_TRUE_TARGET = nullptr;

}  // namespace JMHB
