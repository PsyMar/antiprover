// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implementation for the Piece class.
 * @version Antiprover 0.0.2
 * @date 2021-01-17
 * @since Antiprover 0.0.2 2021-01-17
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#include "../headers/piece.hpp"

#include <stdexcept>

#include "../headers/piecetype.hpp"
#include "../headers/player.hpp"

namespace JMHB {
namespace Chess {

constexpr Piece::Piece(const PieceType& pt, const Player& pl)
    : my_type(pt), my_player(pl) {}

constexpr Piece::Piece(const Player& pl, const PieceType& pt)
    : my_type(pt), my_player(pl) {}

constexpr PieceType Piece::type() const { return my_type; }

constexpr Piece::operator PieceType() const { return my_type; }

constexpr Player Piece::player() const { return my_player; }

constexpr Piece::operator Player() const { return my_player; }

constexpr char Piece::to_FEN_char() const {
  if (Player::WHITE() == my_player) {
    return my_type.to_upper_char();
  } else {
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 9
    if (Player::BLACK() == my_player) {
      return my_type.to_lower_char();
    }
#endif
    return my_type.to_lower_char();
  }
}

constexpr char Piece::to_filename_char() const {
  if (Player::WHITE() == my_player) {
    return my_type.to_upper_char();
  } else {
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 9
    if (Player::BLACK() == my_player) {
      return my_type.to_rot13_char();
    }
#endif
    return my_type.to_rot13_char();
  }
}

constexpr Piece::Piece(char ch)
    : my_type(PieceType(ch)),
      my_player(Player::WHITE()) { /* player must be constructed nondefault */
  switch (ch) {
    case 'B':
    case 'K':
    case 'N':
    case 'Q':
    case 'R':
    case 'P':
      break;
    case 'b':
    case 'k':
    case 'n':
    case 'q':
    case 'r':
    case 'p':
    case 'a':
    case 'c':
    case 'd':
    case 'e':
    case 'o':
    case 'x':
      my_player = Player::BLACK();
      break;
    default:
      throw std::invalid_argument(
          static_cast<std::string>(
              "upper-case ROT13'd char passed to Piece(char): ") +
          ch);
  }
}

constexpr bool Piece::operator==(const Piece& rha) const {
  return ((type() == rha.type()) && (player() == rha.player()));
}

constexpr bool Piece::operator!=(const Piece& rha) const {
  return ((type() != rha.type()) || (player() != rha.player()));
}

/** @var Piece::my_type
 * @brief The type of this Piece.
 * @since Antiprover 0.0.2 2021-01-17
 * @var Piece::my_player
 * @brief The Player owning this Piece.
 * @since Antiprover 0.0.2 2021-01-17 */

}  // namespace Chess
}  // namespace JMHB
