// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implementation for the square class.
 * @version Antiprover 0.0.2
 * @date 2021-01-23
 * @since Antiprover 0.0.1 2021-01-23
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#include "../headers/square.hpp"

#include <array>
#include <bit>
#include <stdexcept>

#include "../headers/miscutils.hpp"
#include "../headers/square.hpp"

namespace JMHB {
namespace Chess {

#if 0
/** @brief Convert a single-bit bitboard to a square.
 * @since Antiprover 0.0.1 2021-01-04 */
Square to_square(const bitboard_t &b) {
#ifndef NDEBUG
  if (!std::has_single_bit(b)) {
    throw std::runtime_error(std::to_string(b) + " has more than one bit set!");
  }
#endif /* NDEBUG */
  return static_cast<Square>(std::countr_zero(b));
}
#endif /* 0 */

/** @brief An array of bitboards representing each square.
 * @since Antiprover 0.0.1 2021-01-04 */
/* initializing expression generated with the following python:
for i in range(64):
    print("    static_cast<bitboard_t>(UINT64_C(1) << " + str(i) + "),")
 */
constexpr std::array<bitboard_t, Square::NUM_SQUARES> SQUARE_BOARDS = {
    static_cast<bitboard_t>(UINT64_C(1)),
    static_cast<bitboard_t>(UINT64_C(1) << 1),
    static_cast<bitboard_t>(UINT64_C(1) << 2),
    static_cast<bitboard_t>(UINT64_C(1) << 3),
    static_cast<bitboard_t>(UINT64_C(1) << 4),
    static_cast<bitboard_t>(UINT64_C(1) << 5),
    static_cast<bitboard_t>(UINT64_C(1) << 6),
    static_cast<bitboard_t>(UINT64_C(1) << 7),
    static_cast<bitboard_t>(UINT64_C(1) << 8),
    static_cast<bitboard_t>(UINT64_C(1) << 9),
    static_cast<bitboard_t>(UINT64_C(1) << 10),
    static_cast<bitboard_t>(UINT64_C(1) << 11),
    static_cast<bitboard_t>(UINT64_C(1) << 12),
    static_cast<bitboard_t>(UINT64_C(1) << 13),
    static_cast<bitboard_t>(UINT64_C(1) << 14),
    static_cast<bitboard_t>(UINT64_C(1) << 15),
    static_cast<bitboard_t>(UINT64_C(1) << 16),
    static_cast<bitboard_t>(UINT64_C(1) << 17),
    static_cast<bitboard_t>(UINT64_C(1) << 18),
    static_cast<bitboard_t>(UINT64_C(1) << 19),
    static_cast<bitboard_t>(UINT64_C(1) << 20),
    static_cast<bitboard_t>(UINT64_C(1) << 21),
    static_cast<bitboard_t>(UINT64_C(1) << 22),
    static_cast<bitboard_t>(UINT64_C(1) << 23),
    static_cast<bitboard_t>(UINT64_C(1) << 24),
    static_cast<bitboard_t>(UINT64_C(1) << 25),
    static_cast<bitboard_t>(UINT64_C(1) << 26),
    static_cast<bitboard_t>(UINT64_C(1) << 27),
    static_cast<bitboard_t>(UINT64_C(1) << 28),
    static_cast<bitboard_t>(UINT64_C(1) << 29),
    static_cast<bitboard_t>(UINT64_C(1) << 30),
    static_cast<bitboard_t>(UINT64_C(1) << 31),
    static_cast<bitboard_t>(UINT64_C(1) << 32),
    static_cast<bitboard_t>(UINT64_C(1) << 33),
    static_cast<bitboard_t>(UINT64_C(1) << 34),
    static_cast<bitboard_t>(UINT64_C(1) << 35),
    static_cast<bitboard_t>(UINT64_C(1) << 36),
    static_cast<bitboard_t>(UINT64_C(1) << 37),
    static_cast<bitboard_t>(UINT64_C(1) << 38),
    static_cast<bitboard_t>(UINT64_C(1) << 39),
    static_cast<bitboard_t>(UINT64_C(1) << 40),
    static_cast<bitboard_t>(UINT64_C(1) << 41),
    static_cast<bitboard_t>(UINT64_C(1) << 42),
    static_cast<bitboard_t>(UINT64_C(1) << 43),
    static_cast<bitboard_t>(UINT64_C(1) << 44),
    static_cast<bitboard_t>(UINT64_C(1) << 45),
    static_cast<bitboard_t>(UINT64_C(1) << 46),
    static_cast<bitboard_t>(UINT64_C(1) << 47),
    static_cast<bitboard_t>(UINT64_C(1) << 48),
    static_cast<bitboard_t>(UINT64_C(1) << 49),
    static_cast<bitboard_t>(UINT64_C(1) << 50),
    static_cast<bitboard_t>(UINT64_C(1) << 51),
    static_cast<bitboard_t>(UINT64_C(1) << 52),
    static_cast<bitboard_t>(UINT64_C(1) << 53),
    static_cast<bitboard_t>(UINT64_C(1) << 54),
    static_cast<bitboard_t>(UINT64_C(1) << 55),
    static_cast<bitboard_t>(UINT64_C(1) << 56),
    static_cast<bitboard_t>(UINT64_C(1) << 57),
    static_cast<bitboard_t>(UINT64_C(1) << 58),
    static_cast<bitboard_t>(UINT64_C(1) << 59),
    static_cast<bitboard_t>(UINT64_C(1) << 60),
    static_cast<bitboard_t>(UINT64_C(1) << 61),
    static_cast<bitboard_t>(UINT64_C(1) << 62),
    static_cast<bitboard_t>(UINT64_C(1) << 63),
};

}  // namespace Chess
}  // namespace JMHB
