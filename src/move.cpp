#if 0
// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implements some of the stuff from move.h and documents private
 * members.
 * @version Antiprover 0.0.1
 * @date 2021-01-03
 * @since Antiprover 0.0.1 2021-01-01
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file.
 */

#include "../headers/move.h"

#include <string>

Move::Move(Square fromsq, Square tosq, Piece promoted_to = Piece::NONE)
    : from_sq(fromsq), to_sq(tosq), promote_to(clear_color(promoted_to)) {
#ifndef NDEBUG
  if (!invariant()) {
    std::string hwaet = "Invalid arguments to Move::Move!\nFrom square:  ";
    hwaet += static_cast<std::string>(fromsq) + "\nTo square:  ";
    hwaet += static_cast<std::string>(tosq) + "\nPromoted to:  ";
    hwaet += static_cast<std::string>(promoted_to);
    throw std::invalid_argument(hwaet);
  }
#endif
}

Square Move::start() const { return from_sq; }

Square Move::from() const { return from_sq; }

Square Move::end() const { return to_sq; }

Square Move::to() const { return to_sq; }

bool Move::is_promotion() const { return promote_to != Piece::NONE; }

Piece Move::promotion() const { return promote_to; }

/** @brief Test the class invariant.
 * @see Move
 * @since Antiprover 0.0.1 2021-01-01
 */
bool Move::invariant() const {
  return ((from() == start()) && (end() == to()) && (from() != to()) &&
          (promotion() != Piece::PAWN) && (!(promotion() & Piece::COLOR_FLAG)));
}

BitpackedMove::BitpackedMove(const Move& mv) {
  uint16_t frombits = static_cast<uint8_t>(static_cast<square_t>(mv.from()));
  uint16_t tobits = static_cast<uint8_t>(static_cast<square_t>(mv.to()));
  uint16_t prombits =
      static_cast<uint8_t>(static_cast<piece_t>(mv.promotion()));
  /* these *need* to be uint16s, not uint8s, or this doesn't work */
  mybits = (prombits << 12) + (frombits << 6) + (tobits);
}

Move BitpackedMove::expand() const { return Move(from(), to(), prom()); }

static Move Move::extract(const BitpackedMove& BM) { return BM.expand(); }

/** @brief Extract the square the move is from.
 * @since Antiprover 0.0.1 2021-01-03
 */
Square BitpackedMove::from() const {
  return static_cast<Square>(static_cast<uint8_t>(mybits >> 6) &
                             static_cast<uint8_t>(077));
}

/** @brief Extract the square the move is to.
 * @since Antiprover 0.0.1 2021-01-03
 */
Square BitpackedMove::to() const {
  return static_cast<Square>(
      static_cast<uint8_t>((mybits) & static_cast<uint8_t>(077)));
}

/** @brief Extract the piece being promoted to, if any, else Piece::NONE.
 * @since Antiprover 0.0.1 2021-01-03
 */
Piece BitpackedMove::prom() const {
  return static_cast<Piece>((mybits >> 12 /* 4*3 -> four octits */) &
                            PIECE_MASK);
}

/** @brief Test the class invariant.
 * @details The class invariant is, from an external point of view,
 * that a Move extracted from this class will satisfy Move's invariant.
 * It is however enforced as from() and to() returning different squares,
 * and prom() never returning Piece::PAWN or a piece that is colored
 * Piece::BLACK.
 * @since Antiprover 0.0.1 2021-01-03
 */
bool BitpackedMove::invariant() const {
  return ((from() != to()) && (prom() != Piece::PAWN) &&
          (!(prom() & Piece::COLOR_FLAG)));
}

uint16_t BitpackedMove::to_bits() const { return mybits; }

/** @brief Constructor used by from_bits(), not publically accessible.
 * @since Antiprover 0.0.1 2021-01-02
 * @see BitpackedMove::from_bits(uint16_t)
 */
BitpackedMove::BitpackedMove(uint16_t bits) {
  mybits = bits;
#ifndef NDEBUG
  if (!invariant()) {
    std::string hwaet =
        "Invalid argument to BitpackedMove::BitpackedMove(uint16_t): ";
    hwaet += static_cast<std::string>(bits); /* as an integer */
    throw std::invalid_argument(hwaet);
  }
#endif
}

static BitpackedMove BitpackedMove::from_bits(uint16_t bits) {
  return BitpackedMove(bits);
}

/**
 * @var Move::from_sq
 * @brief The square the move is from.
 * @since Antiprover 0.0.1 2021-01-01
 * @var Move::to_sq
 * @brief The square the move is to.
 * @since Antiprover 0.0.1 2021-01-01
 * @var Move::promote_to
 * @brief The piece being promoted to, if any, otherwise Piece::NONE.
 * @since Antiprover 0.0.1 2021-01-01
 *
 * @var BitpackedMove::mybits
 * @brief Storage for the BitpackedMove type.
 * @details Numbering the bits as FEDCBA98 76543210:
 * bit F should always be off.
 * bits E-C encode the promotion piece.
 * bits B-6 encode the square being moved from.
 * bits 5-0 encode the square being moved to.
 * @since Antiprover 0.0.1 2021-01-03
 */
#endif
