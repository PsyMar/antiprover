// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implementation for the Player class.
 * @version Antiprover 0.0.2
 * @date 2021-01-17
 * @since Antiprover 0.0.1 2021-01-14
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file.
 */

#include "../headers/player.hpp"

#include <stdexcept>

#include "../headers/0compile_time_options.hpp"
#include "../headers/piece.hpp"

namespace JMHB {
namespace Chess {

constexpr Player Player::WHITE() { return Player(false); }

constexpr Player Player::BLACK() { return Player(true); }

constexpr Player::operator char() const {
  if (BLACK() == *this) {
    return 'b';
  } else {
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 9
    if (WHITE() == *this) {
      return 'w';
    }
#else
    return 'w';
#endif
  }
}

constexpr Player::Player(char ch) {
  if ('w' == ch || 'W' == ch) {
    *this = WHITE();
  } else if ('b' == ch || 'B' == ch) {
    *this = BLACK();
  } else {
    throw std::invalid_argument(
        static_cast<std::string>(
            "invalid character passed to Player(char):  ") +
        ch);
  }
}

constexpr bool Player::operator==(const Player& rha) const {
  return (is_black == rha.is_black);
}

constexpr bool Player::operator!=(const Player& rha) const {
  return (is_black != rha.is_black);
}

constexpr Player Player::operator~() const { return Player(!(this->is_black)); }

constexpr Piece Player::operator&(const PieceType& rha) const {
  return Piece(*this, rha);
}

constexpr Piece Player::operator+(const PieceType& rha) const {
  return Piece(*this, rha);
}

/** @brief Construct a Player.
 * @arg is_black_ true for black false for white.
 * @since Antiprover 0.0.1 2021-01-14 */
constexpr Player::Player(bool is_black_) : is_black(is_black_) {}

/** @var Player::is_black
 * @brief true if player is BLACK false if WHITE.
 * @since Antiprover 0.0.1 2021-01-14 */

}  // namespace Chess
}  // namespace JMHB
