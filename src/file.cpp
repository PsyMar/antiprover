// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implementation for the File class.
 * @version Antiprover 0.0.2
 * @date 2021-01-27
 * @since Antiprover 0.0.2 2021-01-19
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#include "../headers/file.hpp"

#include <array>
#include <stdexcept>
#include <string>

#include "../headers/0compile_time_options.hpp"
#include "../headers/miscutils.hpp"

namespace JMHB {
namespace Chess {

constexpr File::File(char ch) : value(ch - 'a') {
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 5
  if (!is_valid()) {
    throw std::logic_error(
        "attempted to convert invalid char to File!  char was ASCII " +
        std::to_string(static_cast<int>(ch)));
  }
#endif
}

/** @brief Add a File and a File::Delta
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr File File::operator+(const Delta& rha) const {
  return File(value + rha.value);
}

/** @brief add a File::Delta to a File and assign
 * @since Antiprover 0.0.2 2021-01-20 */
File& File::operator+=(const Delta& rha) {
  value += rha.value;
  return *this;
}

/** @brief prefix increment a File
 * @since Antiprover 0.0.2 2021-01-20 */
File& File::operator++() {
  ++value;
  return *this;
} /* prefix */

/** @brief postfix increment a File
 * @since Antiprover 0.0.2 2021-01-20 */
File File::operator++(int) /* postfix */ {
  File copythis(value);
  ++value;
  return copythis;
}

/** @brief unary plus does nothing
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr File File::operator+() const /* unary */ { return *this; }

/** @brief subtract a File::Delta from a File
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr File File::operator-(const Delta& rha) const {
  return File(value - rha.value);
}

/** @brief Subtract a File from a File, giving a File::Delta
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr File::Delta File::operator-(const File& rha) const {
  return Delta(value - rha.value);
}

/** @brief Subtract File::Delta from File and assign to lha
 * @since Antiprover 0.0.2 2021-01-20 */
File& File::operator-=(const Delta& rha) {
  value -= rha.value;
  return *this;
}

/** @brief prefix decrement a File
 * @since Antiprover 0.0.2 2021-01-20 */
File& File::operator--() /* prefix */ {
  --value;
  return *this;
}

/** @brief postfix decrement a File
 * @since Antiprover 0.0.2 2021-01-20 */
File File::operator--(int) /* postfix */ {
  File copythis(value);
  --value;
  return copythis;
}

/* there is no unary minus as there are no negative Files */
/* there are no multiply or divides or mods.  how do you multiply a File */

constexpr File::operator char() const {
  /* you should have already tested is_valid() */
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 5
  if (!is_valid()) {
    throw std::logic_error(
        "attempted to convert invalid File to char!  File value was " +
        std::to_string(value));
  }
#endif
  return value + 'a';
}

constexpr void* File::is_valid() const {
  if (value >= 0 && value < NUM_FILES) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

constexpr void* File::is_valid(char ch) {
  if (ch >= 'a' && ch <= 'h') {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

constexpr File::operator void*() const {
  if (value >= 0 && value < NUM_FILES) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Return false if the File is valid, and true if it is not.
 * void * implicit converts to bool, but unlike bool does not implicit convert
 * to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* File::operator!() const {
  if (value >= 0 && value < NUM_FILES) {
    return VOID_FALSE;
  } else {
    return VOID_TRUE;
  }
}

/** @brief compare two Files for equality
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr void* File::operator==(const File& rha) const {
  if (value == rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief compare two Files for inequality
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr void* File::operator!=(const File& rha) const {
  if (value != rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief A bitboard representing the squares of the first File.
 * @since Antiprover 0.0.2 2021-01-20 */     /*1234567812345678*/
constexpr bitboard_t FILE_1_BOARD = UINT64_C(0x00000000000000FF);

/** @brief An array of bitboards representing the squares of each File.
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr std::array<bitboard_t, File::NUM_FILES> FILE_BOARDS = {
    FILE_1_BOARD,
    FILE_1_BOARD << (1 * File::NUM_FILES),
    FILE_1_BOARD << (2 * File::NUM_FILES),
    FILE_1_BOARD << (3 * File::NUM_FILES),
    FILE_1_BOARD << (4 * File::NUM_FILES),
    FILE_1_BOARD << (5 * File::NUM_FILES),
    FILE_1_BOARD << (6 * File::NUM_FILES),
    FILE_1_BOARD << (7 * File::NUM_FILES)};

constexpr bitboard_t File::bitboard() const {
  /* you really should have already checked is_valid */
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 5
  if (!is_valid()) {
    throw std::logic_error(
        "attempted to convert invalid File to bitboard!  File value was " +
        std::to_string(value));
  }
#endif
  return FILE_BOARDS[value];
}

/** @brief Create a File from an index.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::File(index_t idx) : value(idx) {}

constexpr File::Delta File::Delta::zero() { return Delta(); }

constexpr File::Delta File::Delta::right() { return Delta(1); }

constexpr File::Delta File::Delta::left() { return Delta(-1); }

/** @brief add two File::Delta
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator+(const File::Delta& rha) const {
  return Delta(value + rha.value);
}

/** @brief add a File::Delta to a File
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File File::Delta::operator+(const File& rha) const {
  return File(rha.value + value);
}

/** @brief add two File::Delta, assign to lha
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator+=(const File::Delta& rha) {
  value += rha.value;
  return *this;
}

/** @brief prefix increment.  equivalent to += File::Delta::right()
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator++() {
  value += 1;
  return *this;
}

/** @brief postfix increment.  adds File::Delta::right() but returns old value
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta File::Delta::operator++(int) {
  Delta copythis(value);
  value += 1;
  return copythis;
}

/** @brief unary plus is a noop
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator+() const { return *this; }

/** @brief difference in File::Delta
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator-(const File::Delta& rha) const {
  return Delta(value - rha.value);
}

/** @brief difference in File::Delta, assign to lha
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator-=(const File::Delta& rha) {
  value -= rha.value;
  return *this;
}

/** @brief prefix decrement.  equivalent to += File::Delta::left()
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator--() {
  value -= 1;
  return *this;
}

/** @brief postfix decrement.  adds File::Delta::left() but returns old value
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta File::Delta::operator--(int) {
  Delta copythis(value);
  value -= 1;
  return copythis;
}

/** @brief unary minus -> reverse direction
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator-() const { return Delta(-(value)); }

/** @brief multiply by a scalar
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator*(int rha) const {
  return Delta(value * rha);
}

/** @brief multiply by scalar and assign
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator*=(int rha) {
  value *= rha;
  return *this;
}

/** @brief multiply by a scalar, other way around
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta operator*(int lha, const File::Delta& rha) {
  return rha * lha;
}

/** @brief *integer* division.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator/(int rha) const {
  return Delta(value / rha);
}

/** @brief *integer* division.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr int File::Delta::operator/(const File::Delta& rha) const {
  return value / rha.value;
}

/** @brief *integer* division and assign to lha.
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator/=(int rha) {
  value /= rha;
  return *this;
}

/** @brief *integer* modulo, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator%(const int& rha) const {
  return Delta(value % rha);
}

/** @brief *integer* modulo, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta File::Delta::operator%(const File::Delta& rha) const {
  return Delta(value % rha.value);
}

/** @brief *integer* modulo-and-assign, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator%=(const int& rha) {
  value %= rha;
  return *this;
}

/** @brief *integer* modulo-and-assign, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
File::Delta& File::Delta::operator%=(const File::Delta& rha) {
  value %= rha.value;
  return *this;
}

/** @brief Convert to nullptr or non-null ptr based on is it zero or not
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta::operator void*() const {
  if (value != 0) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}
/** @brief Convert to nullptr or non-null ptr based on is it nonzero or zero
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* File::Delta::operator!() const {
  if (value == 0) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Check two Deltas for equality.
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* File::Delta::operator==(const Delta& rha) const {
  if (value == rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Check two Deltas for inequality.
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* File::Delta::operator!=(const Delta& rha) const {
  if (value != rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Construct a delta of zero, i.e. no change.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta::Delta() : value(0) {} /* zero */

/** @brief Construct a File delta from an int.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr File::Delta::Delta(int somenum) : value(somenum) {}

/** @var File::Delta::value
 * @brief storage for the value of the delta between two Files
 * @since Antiprover 0.0.2 2021-01-19
 * @var File::value
 * @brief storage for the index of a File
 * @since Antiprover 0.0.2 2021-01-20 */

}  // namespace Chess
}  // namespace JMHB
