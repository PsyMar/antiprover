#if 0 
// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implements some of the stuff from board.h and documents private
 * members.
 * @version Antiprover 0.0.1
 * @date 2021-01-04
 * @since Antiprover 0.0.1 2021-01-04
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file.
 */

#include "../headers/board.h"

#include <iostream>
#include <locale>

#include "../headers/board_constants.h"

Board::Board(const std::string& FEN) {
  /* to save you time, here's a python function that *for valid inputs*
  should give the same output:
  def stringtoboard(str):
      str = str.strip().split(' ')
      board = str[0].split('/')
      pieces = [None]*NUM_SQUARES
      for rank in range(0,64,8):
          pieces[rank:rank+8] = [char_to_piece[char] for char in board[rank//8]]
          # char_to_piece might even be the identity, in python
      black_to_move = (str[1][0] in 'Bb')
      epfile = (None if (str[2][0] == '-') else (char_to_file[str[2][0]])
      # char_to_file might, again, be the identity
      movect50 = int(str[3])
      return Board(piecearray=pieces, black_to_move=black_to_move, \
          move_counter_50 = movect50, en_passant = epfile)     */
  std::string goodchars = "12345678BKNPQRbknpqr";
  /* only these should start each rank */
  int startloc = FEN.find_first_of(goodchars);
  if (startloc == std::string::npos) {
    std::string errmsg = "Invalid FEN passed to Board::Board: ";
    errmsg += FEN;
    throw std::invalid_argument(errmsg);
  }
  std::array<Piece, Board::NUM_SQUARES> pieces;
  square_t rank0 = static_cast<square_t>(Rank::_1) - 1;
  /* since square_t is unsigned this is 255 */
  for (square_t ranknum = Rank::_8; ranknum != rank0; --ranknum) {
    /* carefully testing against 255 there lol */
    /* we have to use square_t since rank can't hold non-rank values */
    Rank rank = static_cast<Rank>(ranknum);
    /* if present, skip '/' from after previous rank */
    startloc = FEN.find_first_of(goodchars, startloc);
    try { /* convert 1-8 characters into 8 Piece s and store in array */
      startloc = parse_FEN_rank(FEN, pieces, rank, startloc);
      /* return value is first unparsed index */
    } catch (std::invalid_argument& e) {
      std::string errmsg = e.what();
      errmsg += "\nInvalid FEN passed to Board::Board: ";
      errmsg += FEN;
      errmsg += "\nError found on rank: ";
      errmsg += to_char(rank);
      throw std::invalid_argument(errmsg);
    }
  } /* that's the squares done! */
  /* ASSUMPTION: if no whose turn char, also no En Passant or 50-move count */
  goodchars = "WwBb";
  startloc = FEN.find_first_of(goodchars, startloc);
  if (startloc == std::string::npos) {
    /* just assume White to move, no en passant, no 50-move count */
    *this = Board(pieces);
    return;
  }
  bool black_to_move;
  if ((FEN[startloc] == 'w') || (FEN[startloc] == 'W')) {
    /* TECHNICALLY 'W' is invalid but we'll let it slide. */
    black_to_move = false;
  } else if ((FEN[startloc] == 'b') || (FEN[startloc] == 'B')) {
    /* TECHNICALLY 'B' is invalid but we'll allow it. */
    black_to_move = true;
  } else {
    std::cerr << "BIGTIME UHOH #1 in Board::Board(string) with FEN = ";
    std::cerr << FEN << std::endl << std::flush;
    /* this is not very nice, but would indicate a serious programmatic fail */
    assert("+++OUT OF CHEESE ERROR+++" && false);
  }
  /* black to move has been found! next *should* be castling rights or EP. */
  /* really *should* be castlerights, but since anti doesn't have castling... */
  goodchars = "-abcdefghABCDEFGH"; /* uppercase shouldn't be allowed here but */
  startloc = FEN.find_first_of(goodchars, startloc + 1);
  /* that +1 doin' work to skip the b from black-to-move lol */
  if (startloc == std::string::npos) {
    /* no castling, ep, or 50-counter */
    *this = Board(pieces, black_to_move);
    return;
  }
  /* if FEN[startloc] is a..h we're there already */
  /* but if hyphen, it could be castling or it could be EP */
  if (FEN[startloc] == '-') { /* the special case, also what *should* happen */
    /* because the castling rights should be present as a hyphen */
    /* but in case not.... */
    int epstart = FEN.find_first_of("abcdefghABCDEFGH", startloc + 1);
    /* upper-case shouldn't be allowed here but.  it could happen */
    if (epstart != std::string::npos) {
      /* the hyphen at startloc IS castling and ep is not hyphen */
      startloc = epstart;
    }
  }                           /* no else here.  startloc may have changed! */
  if (FEN[startloc] != '-') { /* a-h */
    /* can ignore 50-move counter because it has to be 0 if e.p. legal */
    *this = Board(pieces, black_to_move, 0, to_file(FEN[startloc]));
    return;
  }
  /* if we're still here, no en passant. */
  /* now we look for a digit for the 50-move counter. */
  /* it comes *before* not after the full-move counter. */
  goodchars = "0123456789";
  startloc = FEN.find_first_of(goodchars, startloc + 1);
  if (startloc == std::string::npos) {
    /* assume no 50-move count */
    *this = Board(pieces, black_to_move);
    return;
  }
  int endloc = FEN.find_first_not_of(goodchars, startloc);
  /* if endloc is npos, substr works anyway! */
  std::string fiftymove = FEN.substr(startloc, endloc - startloc + 1);
  int fiftymovenum = std::stoi(fiftymove);
  if (fiftymovenum > 100) {
    fiftymovenum = 100;
  }
  *this = Board(pieces, black_to_move, fiftymovenum);
}

/** @var Board::black_pieces
 * @brief Information as to where the black pieces are.
 * @details black_pieces[ALL] gives an OR-ing of all black pieces.
 * black_pieces[BISHOP] through black_pieces[PAWN] give the relevant bitboards.
 * @since Antiprover 0.0.1 2021-01-03
 * @var Board::white_pieces
 * @brief Information as to where the white pieces are.
 * @details white_pieces[ALL] gives an OR-ing of all white pieces.
 * white_pieces[BISHOP] through white_pieces[PAWN] give the relevant bitboards.
 * @since Antiprover 0.0.1 2021-01-03
 * @var Board::ep_file
 * @brief If en passant is legal -- on which file?  Otherwise std::nullopt.
 * @since Antiprover 0.0.1 2021-01-03
 * @var Board::hash
 * @brief A hash of the current board state, excluding the 50-move counter.
 * @since Antiprover 0.0.1 2021-01-03
 * @var Board::hundred_ply_count
 * @brief The number of ply without a capture or pawn move.
 * @details At 100 ply, either side may claim a draw.
 * @since Antiprover 0.0.1 2021-01-03
 * @var Board::black_to_move
 * @brief True if black is to move, false is White is to move, simple right?
 * @since Antiprover 0.0.1 2021-01-03
 *
 * @var Smallboard::allwhiteboard
 * @brief A bitboard of all the white pieces.
 * @details Equivalent to Board::white_pieces[ALL].
 * @since Antiprover 0.0.1 2021-01-03
 * @var Smallboard::btm100ep
 * @brief Holds side-to-move, 100-ply counter, and en passant file -- in one
 * byte!
 * @details MSB is side to move.  The other 7 bits encode a number in 0..100 or
 * 120..127.  0..100 is the relevant 100 ply counter with no en passant allowed.
 * 120..127 is an en passant file of File(0..7) 100-ply count of 0 (because the
 * last move was, obviously, a pawn move!)
 * @note I'm very proud of that little hack.  Saves a whole byte.
 * @since Antiprover 0.0.1 2021-01-03
 * @var Smallboard::knpboard
 * @brief Bitboard for kings, knights, and pawns.
 * @details Smallboard::qbnboard is true for knights, and Smallboard::qrkboard
 * * is true for kings.  Neither is true for pawns.  If all three true, error.
 * If all three false, the square is empty.
 * @see Smallboard::qrkboard
 * @see Smallboard::qbnboard
 * @since Antiprover 0.0.1 2021-01-03
 * @var Smallboard::qbnboard
 * @brief Bitboard for queens, bishops, and knights.
 * @details Smallboard::qrkboard is true for queens, and Smallboard::knpboard
 * is true for knights.  Neither is true for bishops.  If all three true, error.
 * If all three false, the square is empty.
 * @see Smallboard::knpboard
 * @see Smallboard::qrkboard
 * @since Antiprover 0.0.1 2021-01-03
 * @var Smallboard::qrkboard
 * @brief Bitboard for queens, rooks, and kings.
 * @details Smallboard::qbnboard is true for queens, and Smallboard::knpboard
 * is true for kings.  Neither is true for rooks.  If all three true, error.
 * If all three false, the square is empty.
 * @see Smallboard::knpboard
 * @see Smallboard::qbnboard
 * @since Antiprover 0.0.1 2021-01-03
 */

#if 0 /* leftover ep-handling code.  ignore for now */
   if(ep_legal) { /* 50-move counter must be zero, because last move was pawn! */
        /* these ?:s are reversed from what you think, because we're looking at */
        /* the LAST move, not the NEXT move which black_to_move is based on */
        Rank pawn_move_start_rank = black_to_move ? (Rank::_2) : (Rank::_7);
        Rank pawn_move_middle_rank = black_to_move ? (Rank::_3) : (Rank::_6);
        Rank pawn_move_end_rank = black_to_move ? (Rank::_4) : (Rank::_5);
        Square pawn_move_start_square = to_square(pawn_move_start_rank, ep_file);
        Square pawn_move_middle_square = to_square(pawn_move_middle_rank, ep_file);
        Square pawn_move_end_square = to_square(pawn_move_end_rank, ep_file);
        Move the_pawn_move(pawn_move_start_square, pawn_move_end_square);
        Piece colored_pawn = Piece::PAWN |
            (black_to_move ? (Piece::WHITE) : (Piece::BLACK));
        if (pieces[pawn_move_end_square] != colored_pawn) {
            /* oops. */
            string errmsg = "Invalid en passant file: ";
            errmsg += to_char(ep_file);
            errmsg += "\nin FEN string: ";
            errmsg += FEN;
            errmsg += "\nNo pawn on correct square for e.p. capture!\n";
            throw invalid_argument(errmsg);
        } else if (pieces[pawn_move_start_square] != Piece::NONE ||
            pieces[pawn_move_middle_square != Piece::NONE) {
            /* oops again */
            string errmsg = "Invalid en passant file: ";
            errmsg += to_char(ep_file);
            errmsg += "\nin FEN string: ";
            errmsg += FEN;
            errmsg += "\nTwo squares behind pawn not empty!\n";
            throw invalid_argument(errmsg);
        } /* and with that taken care of */
        /* undo the pawn move */
        pieces[pawn_move_start_square] = colored_pawn;
        pieces[pawn_move_end_square] = Piece::NONE;
        black_to_move = !black_to_move;
        /* now make the board */
        Board retboard(pieces, black_to_move);
        /* play the pawn move */
        retboard.make_move(the_pawn_move);
        return retboard;
    } /* en passant dealt with!!! */
#endif
#endif
