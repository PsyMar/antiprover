// This file is part of Antiprover, a suite for playing and generating proof
// trees for antichess. Copyright (C) 2021 JMHB, contact address
// cflzne_tvgyno@fastmail.com.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/** @file
 * @brief Implementation for the Rank class.
 * @version Antiprover 0.0.2
 * @date 2021-01-19
 * @since Antiprover 0.0.2 2021-01-19
 * @author JMHB, cflzne_tvgyno@fastmail.com
 * @copyright see notice at the top of this file. */

#include "../headers/rank.hpp"

#include <array>
#include <stdexcept>
#include <string>

#include "../headers/0compile_time_options.hpp"
#include "../headers/miscutils.hpp"

namespace JMHB {
namespace Chess {

constexpr Rank::Rank(char ch) : value(ch - '1') {
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 5
  if (!is_valid()) {
    throw std::logic_error(
        "attempted to convert invalid char to Rank!  char was ASCII " +
        std::to_string(static_cast<int>(ch)));
  }
#endif
}

/** @brief Add a Rank and a Rank::Delta
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr Rank Rank::operator+(const Delta& rha) const {
  return Rank(value + rha.value);
}

/** @brief add a Rank::Delta to a Rank and assign
 * @since Antiprover 0.0.2 2021-01-20 */
Rank& Rank::operator+=(const Delta& rha) {
  value += rha.value;
  return *this;
}

/** @brief prefix increment a Rank
 * @since Antiprover 0.0.2 2021-01-20 */
Rank& Rank::operator++() {
  ++value;
  return *this;
} /* prefix */

/** @brief postfix increment a rank
 * @since Antiprover 0.0.2 2021-01-20 */
Rank Rank::operator++(int) /* postfix */ {
  Rank copythis(value);
  ++value;
  return copythis;
}

/** @brief unary plus does nothing
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr Rank Rank::operator+() const /* unary */ { return *this; }

/** @brief subtract a Rank::Delta from a Rank
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr Rank Rank::operator-(const Delta& rha) const {
  return Rank(value - rha.value);
}

/** @brief Subtract a Rank from a Rank, giving a Rank::Delta
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr Rank::Delta Rank::operator-(const Rank& rha) const {
  return Delta(value - rha.value);
}

/** @brief Subtract Rank::Delta from Rank and assign to lha
 * @since Antiprover 0.0.2 2021-01-20 */
Rank& Rank::operator-=(const Delta& rha) {
  value -= rha.value;
  return *this;
}

/** @brief prefix decrement a rank
 * @since Antiprover 0.0.2 2021-01-20 */
Rank& Rank::operator--() /* prefix */ {
  --value;
  return *this;
}

/** @brief postfix decrement a rank
 * @since Antiprover 0.0.2 2021-01-20 */
Rank Rank::operator--(int) /* postfix */ {
  Rank copythis(value);
  --value;
  return copythis;
}

/* there is no unary minus as there are no negative ranks */
/* there are no multiply or divides or mods.  how do you multiply a rank */

constexpr Rank::operator char() const {
  /* you should have already tested is_valid() */
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 5
  if (!is_valid()) {
    throw std::logic_error(
        "attempted to convert invalid Rank to char!  rank value was " +
        std::to_string(value));
  }
#endif
  return value + '1';
}

constexpr void* Rank::is_valid() const {
  if (value >= 0 && value < NUM_RANKS) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

constexpr void* Rank::is_valid(char ch) {
  if (ch >= '1' && ch <= '8') {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

constexpr Rank::operator void*() const {
  if (value >= 0 && value < NUM_RANKS) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Return false if the rank is valid, and true if it is not.
 * void * implicit converts to bool, but unlike bool does not implicit convert
 * to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* Rank::operator!() const {
  if (value >= 0 && value < NUM_RANKS) {
    return VOID_FALSE;
  } else {
    return VOID_TRUE;
  }
}

/** @brief compare two Ranks for equality
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr void* Rank::operator==(const Rank& rha) const {
  if (value == rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief compare two Ranks for inequality
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr void* Rank::operator!=(const Rank& rha) const {
  if (value != rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief A bitboard representing the squares of the first rank.
 * @since Antiprover 0.0.2 2021-01-20 */     /*1234567812345678*/
constexpr bitboard_t RANK_1_BOARD = UINT64_C(0x0101010101010101);

/** @brief An array of bitboards representing the squares of each rank.
 * @since Antiprover 0.0.2 2021-01-20 */
constexpr std::array<bitboard_t, Rank::NUM_RANKS> RANK_BOARDS = {
    RANK_1_BOARD,      RANK_1_BOARD << 1, RANK_1_BOARD << 2, RANK_1_BOARD << 3,
    RANK_1_BOARD << 4, RANK_1_BOARD << 5, RANK_1_BOARD << 6, RANK_1_BOARD << 7};

constexpr bitboard_t Rank::bitboard() const {
  /* you really should have already checked is_valid */
#if PSYMAR_CHESS_ANTI_ALLOW_DEBUG > 5
  if (!is_valid()) {
    throw std::logic_error(
        "attempted to convert invalid Rank to bitboard!  rank value was " +
        std::to_string(value));
  }
#endif
  return RANK_BOARDS[value];
}

/** @brief Create a rank from an index.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Rank(index_t idx) : value(idx) {}

constexpr Rank::Delta Rank::Delta::zero() { return Delta(); }

constexpr Rank::Delta Rank::Delta::up() { return Delta(1); }

constexpr Rank::Delta Rank::Delta::down() { return Delta(-1); }

/** @brief add two Rank::Delta
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator+(const Rank::Delta& rha) const {
  return Delta(value + rha.value);
}

/** @brief add a Rank::Delta to a Rank
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank Rank::Delta::operator+(const Rank& rha) const {
  return Rank(rha.value + value);
}

/** @brief add two Rank::Delta, assign to lha
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator+=(const Rank::Delta& rha) {
  value += rha.value;
  return *this;
}

/** @brief prefix increment.  equivalent to += Rank::Delta::up()
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator++() {
  value += 1;
  return *this;
}

/** @brief postfix increment.  adds Rank::Delta::up() but returns old value
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta Rank::Delta::operator++(int) {
  Delta copythis(value);
  value += 1;
  return copythis;
}

/** @brief unary plus is a noop
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator+() const { return *this; }

/** @brief difference in Rank::Delta
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator-(const Rank::Delta& rha) const {
  return Delta(value - rha.value);
}

/** @brief difference in Rank::Delta, assign to lha
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator-=(const Rank::Delta& rha) {
  value -= rha.value;
  return *this;
}

/** @brief prefix decrement.  equivalent to += Rank::Delta::down()
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator--() {
  value -= 1;
  return *this;
}

/** @brief postfix decrement.  adds Rank::Delta::down() but returns old value
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta Rank::Delta::operator--(int) {
  Delta copythis(value);
  value -= 1;
  return copythis;
}

/** @brief unary minus -> reverse direction
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator-() const { return Delta(-(value)); }

/** @brief multiply by a scalar
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator*(int rha) const {
  return Delta(value * rha);
}

/** @brief multiply by scalar and assign
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator*=(int rha) {
  value *= rha;
  return *this;
}

/** @brief multiply by a scalar, other way around
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta operator*(int lha, const Rank::Delta& rha) {
  return rha * lha;
}

/** @brief *integer* division.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator/(int rha) const {
  return Delta(value / rha);
}

/** @brief *integer* division.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr int Rank::Delta::operator/(const Rank::Delta& rha) const {
  return value / rha.value;
}

/** @brief *integer* division and assign to lha.
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator/=(int rha) {
  value /= rha;
  return *this;
}

/** @brief *integer* modulo, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator%(const int& rha) const {
  return Delta(value % rha);
}

/** @brief *integer* modulo, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta Rank::Delta::operator%(const Rank::Delta& rha) const {
  return Delta(value % rha.value);
}

/** @brief *integer* modulo-and-assign, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator%=(const int& rha) {
  value %= rha;
  return *this;
}

/** @brief *integer* modulo-and-assign, negatives handled per C++ rules for ints
 * @since Antiprover 0.0.2 2021-01-19 */
Rank::Delta& Rank::Delta::operator%=(const Rank::Delta& rha) {
  value %= rha.value;
  return *this;
}

/** @brief Convert to nullptr or non-null ptr based on is it zero or not
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta::operator void*() const {
  if (value != 0) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}
/** @brief Convert to nullptr or non-null ptr based on is it nonzero or zero
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* Rank::Delta::operator!() const {
  if (value == 0) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Check two Deltas for equality.
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* Rank::Delta::operator==(const Delta& rha) const {
  if (value == rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Check two Deltas for inequality.
 * @details void * implicit converts to bool, but unlike bool does not implicit
 * convert to int
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr void* Rank::Delta::operator!=(const Delta& rha) const {
  if (value != rha.value) {
    return VOID_TRUE;
  } else {
    return VOID_FALSE;
  }
}

/** @brief Construct a delta of zero, i.e. no change.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta::Delta() : value(0) {} /* zero */

/** @brief Construct a rank delta from an int.
 * @since Antiprover 0.0.2 2021-01-19 */
constexpr Rank::Delta::Delta(int somenum) : value(somenum) {}

/** @var Rank::Delta::value
 * @brief storage for the value of the delta between two ranks
 * @since Antiprover 0.0.2 2021-01-19
 * @var Rank::value
 * @brief storage for the index of a rank
 * @since Antiprover 0.0.2 2021-01-20 */

}  // namespace Chess
}  // namespace JMHB
